import React from 'react';
import {useApolloContext} from './config/ApolloClient';
import {ThemeContext, getTheme} from 'react-native-material-ui';
import {TabNavigation} from './navigation/tabNavigation';
import {ApolloProvider} from '@apollo/react-hooks';
import {Provider} from 'mobx-react';
import {createStores} from './stores';
import {SafeAreaProvider} from 'react-native-safe-area-context';

const uiTheme = {
  palette: {
    primaryColor: '#E42320',
    accentColor: '#fff',
  },
  toolbar: {
    container: {
      height: 50,
    },
  },
};

declare var global: {HermesInternal: null | {}};
const stores = createStores();

const App = () => {
  const [getClient] = useApolloContext();

  return (
    <SafeAreaProvider>
      <Provider {...stores}>
        <ApolloProvider client={getClient()}>
          <ThemeContext.Provider value={getTheme(uiTheme)}>
            <TabNavigation />
          </ThemeContext.Provider>
        </ApolloProvider>
      </Provider>
    </SafeAreaProvider>
  );
};

export default App;
