import React, {useState} from 'react';
import {createPDF, MyCompanyInvoiceProps} from './createPDF';
import {PDFViewer} from './PDFViewer';
import {Invoice} from 'src/stores/InvoiceStore';
import {View} from 'react-native';
import {
  InvocieStateProps,
  InvoiceAmountProps,
} from '../screens/Invoice/NewInvoiceScreen';

export interface createProps {
  invoice?: Invoice;
  preview?: Boolean;
}

export interface pdfKitStateProps {
  pdfHasFinished: boolean;
  source?: string;
}

export const usePDFKit = () => {
  const [pdfkitState, setPdfkitState] = useState<pdfKitStateProps>({
    pdfHasFinished: false,
    source: undefined,
  });

  const create = async (
    invoiceState: InvocieStateProps,
    company: MyCompanyInvoiceProps,
    invoiceAmount: InvoiceAmountProps,
    languageCode: string,
  ): Promise<void> => {
    let source = await createPDF({
      invoice: invoiceState,
      company,
      invoiceAmount,
      languageCode
    });
    setPdfkitState({...pdfkitState, pdfHasFinished: true, source: source});
    return;
  };
  const resetPDFState = () => {
    setPdfkitState({
      ...pdfkitState, pdfHasFinished: false
    });
  };
  const PresentPDFViewComponent: React.FC = () => {
    return (
      <View>
        {pdfkitState.source && <PDFViewer source={{uri: pdfkitState.source}} />}
      </View>
    );
  };

  return {create, PresentPDFViewComponent, pdfkitState, resetPDFState};
};
