import React from 'react';
import Pdf from 'react-native-pdf';
import {View, StyleSheet, Modal, Text} from 'react-native';
import {WindowDimensions} from '../utils/WindowDimensions';

export interface PDFViewerProps {
  source: Object;
}

export const PDFViewer = ({source}: PDFViewerProps) => {
  return (
    <View style={styles.container}>
      {source !== null && (
        <Pdf
          source={source}
          onLoadComplete={(numberOfPages, filePath) => {
            console.log(`number of pages: ${numberOfPages}`);
          }}
          onPageChanged={(page, numberOfPages) => {
            console.log(`current page: ${page}`);
          }}
          onError={error => {
            console.log(error);
          }}
          onPressLink={uri => {
            console.log(`Link presse: ${uri}`);
          }}
          style={styles.pdf}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //marginTop: 25,
    width: WindowDimensions.width95,
  },
  pdf: {
    flex: 1,
    marginLeft: WindowDimensions.width025,
    width: WindowDimensions.width95,
  },
});
