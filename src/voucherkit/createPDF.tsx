import React from 'react';
import { Image } from 'react-native';
import {PDFDocument, rgb, PDFPage, degrees, PDFFont} from 'pdf-lib';
import fontkit from '@pdf-lib/fontkit';
import RNFS from 'react-native-fs';
import md5 from 'md5';
import {antonRegular} from './fonts/antonregular';
import {montserratRegular} from './fonts/montserratRegular';
import {montserratBold} from './fonts/montserratBold';
import {montserratLight} from './fonts/montserratLight';
import {
  InvocieStateProps,
  InvoiceAmountProps,
  Product
} from '../screens/Invoice/NewInvoiceScreen';
import moment from 'moment';
import { translate } from '../utils/Translate';
import { formatAmount } from '../utils/Amount';
import logo from '../assets/images/paid_logo.png'
import RNFetchBlob from 'rn-fetch-blob';

export interface PDFFontObject {
  [index: string]: PDFFont;
}

export interface MyCompanyInvoiceProps {
  companyName: string;
  companyNumber: string;
  companyEmail: string;
}

export interface CreatePDFProps {
  invoice: InvocieStateProps;
  company: MyCompanyInvoiceProps;
  invoiceAmount: InvoiceAmountProps;
  languageCode: string;
}

export interface GetTextWithAndHeightPops {
  text: string;
  textSize: number;
  font: PDFFont;
}

export interface TableInvoiceStateProps {
  products: Array<Product>;
  page: number;
}

export const createPDF = async ({
  invoice,
  company,
  invoiceAmount,
  languageCode,
}: CreatePDFProps): Promise<string> => {
  return new Promise(async (resolve, reject) => {
    const pdfDoc = await PDFDocument.create();
    //const timesRomanFont = await pdfDoc.embedFont(StandardFonts.TimesRoman);
    pdfDoc.registerFontkit(fontkit);

    const fonts: PDFFontObject = {
      antonRegularFont: await pdfDoc.embedFont(antonRegular),
      montserratRegularFont: await pdfDoc.embedFont(montserratRegular),
      montserratLightFont: await pdfDoc.embedFont(montserratLight),
      montserratBoldFont: await pdfDoc.embedFont(montserratBold),
    };

    //const logoImage = await pdfDoc.embedJpg(testlogo);
    /*
    *
    * It needs some refactoring, but it works.
    * 
    */
    const pages = [];
    //first page
    //this variable is kinda useless, it was here before the shallowcopy "state", and mainly its here for debugging
    let pageNum = 0;
    pages.push(pdfDoc.addPage());
    //headers
    leftLongPanel(pages[pageNum], fonts);
    await printLogo(pdfDoc, pages[pageNum]);
    rigthCompanyDataPanel(pages[pageNum], fonts, company, invoice, languageCode);
    drawIssueTo(pages[pageNum], fonts, invoice, languageCode);
    // I need two of these because its easier to calculate the pages if i have a shallow copy, from which i can remove elements.
    const invoiceCopy = {
      products: [...invoice.products],
      page: pageNum,
    };
    //this is basically just the object we pass as a parameter
    let invoiceTemp = {
      //on the first page we have less space for products
      products: invoiceCopy.products.splice(0, 12),
      page: invoiceCopy.page
    };
    drawTable(pages[invoiceCopy.page], fonts, invoiceTemp, languageCode);
    //other pages
    while(invoiceCopy.products.length > 0)
    {
      invoiceCopy.page++;
      invoiceTemp = {
        //If content is out of the borders of the page because of the comment or discount modify the slice end to 16 or something.
        products: invoiceCopy.products.splice(0, 20),
        page: invoiceCopy.page
      };
      pages.push(pdfDoc.addPage());
      leftLongPanel(pages[invoiceTemp.page], fonts);
      printLogo(pdfDoc, pages[invoiceTemp.page]);
      drawTable(pages[invoiceTemp.page], fonts, invoiceTemp, languageCode);
    }  
    const productNum = invoice.products.length;
    // this is for checking if we are on the pages bottom, if it is we add a blank page so we have space for the amounts
    if ((productNum < 13 && productNum > 8) || (productNum - 12) % 20 > 16) {
      invoiceCopy.page++;
      invoiceTemp = {
        products: [],
        page: invoiceCopy.page
      };
      pages.push(pdfDoc.addPage());
      leftLongPanel(pages[invoiceTemp.page], fonts);
      printLogo(pdfDoc, pages[invoiceTemp.page]);
    }
    drawAmount(pages[invoiceCopy.page], fonts, invoiceTemp, invoice.shipping, invoiceAmount, (invoiceAmount.subTotal * invoice.discountPercent / 100), invoice.comment, languageCode);
    /*
    *
    * end of section, needs refactoring til here.
    * 
    */


    const base64 = await pdfDoc.saveAsBase64();

    let path =
      RNFS.DocumentDirectoryPath + '/' + md5(Math.random().toString()) + '.pdf';
    RNFS.writeFile(path, base64, 'base64')
      .then(() => {
        console.log('FILE WRITTEN!', path);
        resolve(path);
      })
      .catch(() => {
        reject('error');
      });
  });
};

const leftLongPanel = (page: PDFPage, fonts: PDFFontObject): PDFPage => {
  const {width, height} = page.getSize();
  console.log(width, height);
  page.drawRectangle({
    x: 0,
    y: 0,
    width: width / 7.5,
    height: height,
    color: rgb(1, 0, 0),
    //borderWidth: 1.5,
  });
  page.drawText('Invoice', {
    x: 57,
    font: fonts.montserratLightFont,
    y: height - (height - 50),
    color: rgb(1, 1, 1),
    size: 60,
    rotate: degrees(90),
  });
  return page;
};

const printLogo = async (pdfDoc, page) => {
  const logoUri = Image.resolveAssetSource(logo).uri
  const logoBytes = await RNFetchBlob.fetch('get', logoUri);
  const pngLogo = await pdfDoc.embedPng(logoBytes.base64());
  const pngDims = pngLogo.scale(0.1);
  const { width, height } = page.getSize();
  page.drawImage(pngLogo, {
    x: width - width / 25 - pngDims.width * 4/3,
    y:  height / 25 ,
    width: pngDims.width,
    height: pngDims.height,
  });
};
const getTextWithAndHeight = ({
  text,
  textSize,
  font,
}: GetTextWithAndHeightPops) => {
  const textWidth = font.widthOfTextAtSize(text, textSize);
  const textHeight = font.heightAtSize(textSize);

  return [textWidth, textHeight];
};

const rigthCompanyDataPanel = (
  page: PDFPage,
  fonts: PDFFontObject,
  company: MyCompanyInvoiceProps,
  invoice: InvocieStateProps,
  languageCode: string,
): PDFPage => {
  const {width, height} = page.getSize();

  let [textWidth] = getTextWithAndHeight({
    text: 'INVOICE',
    textSize: 45,
    font: fonts.antonRegularFont,
  });

  page.drawText('INVOICE', {
    x: width - 50 - textWidth,
    y: height - 100,
    font: fonts.antonRegularFont,
    color: rgb(0, 0, 0),
    size: 45,
  });

  let [textWidth1] = getTextWithAndHeight({
    text: company.companyName,
    textSize: 18,
    font: fonts.montserratBoldFont,
  });

  page.drawText(company.companyName, {
    x: width - 50 - textWidth1,
    y: height - 150,
    font: fonts.montserratBoldFont,
    color: rgb(0, 0, 0),
    size: 18,
  });

  let [textWidth2] = getTextWithAndHeight({
    text: company.companyNumber,
    textSize: 16,
    font: fonts.montserratRegularFont,
  });

  page.drawText(company.companyNumber, {
    x: width - 50 - textWidth2,
    y: height - 170,
    font: fonts.montserratRegularFont,
    color: rgb(0, 0, 0),
    size: 16,
  });

  let [textWidth3] = getTextWithAndHeight({
    text: company.companyEmail,
    textSize: 16,
    font: fonts.montserratRegularFont,
  });

  page.drawText(company.companyEmail, {
    x: width - 50 - textWidth3,
    y: height - 190,
    font: fonts.montserratRegularFont,
    color: rgb(0, 0, 0),
    size: 16,
  });

  let issueDate = `${translate('term', languageCode)}: ${moment(invoice.payTerm).format(
    'MM/DD/YYYY',
  )}`;

  let [textWidth4] = getTextWithAndHeight({
    text: issueDate,
    textSize: 16,
    font: fonts.montserratRegularFont,
  });

  page.drawText(issueDate, {
    x: width - 50 - textWidth4,
    y: height - 230,
    font: fonts.montserratRegularFont,
    color: rgb(0, 0, 0),
    size: 16,
  });

  let invoiceNumberText = `${translate('invoiceNumber', languageCode)}: ${invoice.invoiceNumber}`;

  let [textWidth5] = getTextWithAndHeight({
    text: invoiceNumberText,
    textSize: 16,
    font: fonts.montserratRegularFont,
  });

  page.drawText(invoiceNumberText, {
    x: width - 50 - textWidth5,
    y: height - 250,
    font: fonts.montserratRegularFont,
    color: rgb(0, 0, 0),
    size: 16,
  });
  return page;
};

const drawTable = (
  page: PDFPage,
  fonts: PDFFontObject,
  invoice: TableInvoiceStateProps,
  languageCode: string
) => {
  const { width, height } = page.getSize();
  const tableStartHeight = (invoice.page == 0) ? height / 2 : height / 10.0 * 8.0;

  let [textWidth_PRICE] = getTextWithAndHeight({
    text: translate('price', languageCode).toUpperCase(),
    textSize: 12,
    font: fonts.montserratBoldFont,
  });

  let lineStart = tableStartHeight;
  let widthStart = 100;
  let columSizes = {
    nr: widthStart + 0,
    name: widthStart + 20,
    quantity: widthStart + 250,
    price: width - 50 - textWidth_PRICE,
  };
  let lineIncreasingValue = 30;

  //header
  const header = {
    nr: '',
    name: translate('productName', languageCode).toUpperCase(),
    quantity: translate('quantity', languageCode).toUpperCase(),
    price: translate('price', languageCode).toUpperCase(),
  };
  Object.keys(header).map(key => {
    page.drawText(header[key], {
      x: columSizes[key],
      y: lineStart + 35,
      font: fonts.montserratBoldFont,
      color: rgb(0, 0, 0),
      size: 12,
    });
  });

  invoice.products.map((product, index) => {
    Object.keys(product).map((key: string) => {
      page.drawText((index + 1 + invoice.page * 12).toString(), {
        x: columSizes.nr,
        y: lineStart,
        font: fonts.montserratRegularFont,
        color: rgb(0, 0, 0),
        size: 12,
      });

      if (key === 'price') {
        let [textWidth] = getTextWithAndHeight({
          text: formatAmount(Math.round(product[key])),
          textSize: 12,
          font: fonts.montserratRegularFont,
        });
        page.drawText(formatAmount(Math.round(product[key])), {
          x: width - 50 - textWidth,
          y: lineStart,
          font: fonts.montserratRegularFont,
          color: rgb(0, 0, 0),
          size: 12,
        });
      } else {
        page.drawText(product[key].toString(), {
          x: columSizes[key],
          y: lineStart,
          font: fonts.montserratRegularFont,
          color: rgb(0, 0, 0),
          size: 12,
        });
      }
    });
    lineStart -= lineIncreasingValue;
  });
};

const drawAmount = (
  page: PDFPage,
  fonts: PDFFontObject,
  invoice: TableInvoiceStateProps,
  shipping: number,
  invoiceAmount: InvoiceAmountProps,
  discount: number,
  comment: string,
  languageCode: string
) => {
  const amounts = [
    {
      name: translate('subTotal', languageCode).toUpperCase(),
      value: formatAmount(Math.round(invoiceAmount.subTotal)),
    },
    ...(discount != 0 ? [{
      name: translate('discount', languageCode).toUpperCase(),
      value: formatAmount(Math.round(discount)),
    }] : []),
    {
      name: translate('tax', languageCode).toUpperCase(),
      value: formatAmount(Math.round(invoiceAmount.tax)),
    },
    ...(shipping != 0 ? [{
      name: translate('shipping', languageCode).toUpperCase(),
      value: formatAmount(Math.round(shipping)),
    }]: []),
    {
      name: translate('total', languageCode).toUpperCase(),
      value: formatAmount(Math.round(invoiceAmount.total)),
    },
  ];

  const {width, height} = page.getSize();
  const tableStartHeight = (invoice.page == 0) ? height / 2 : height / 10.0 * 8.0;

  let lineStart = tableStartHeight - invoice.products.length * 30;
  let widthStart = 100;
  let columSizes = [widthStart + 20, widthStart + 280];
  let lineIncreasingValue = 30;

  amounts.map(amount => {
    Object.values(amount).map((value, key) => {
      let xSpace = columSizes[key];
      if (key === 1) {
        let [amountFontWidth] = getTextWithAndHeight({
          text: value,
          textSize: 14,
          font: fonts.montserratBoldFont,
        });
        xSpace = width - 50 - amountFontWidth;
      }
      page.drawText(value, {
        x: xSpace,
        y: lineStart,
        font: fonts.montserratBoldFont,
        color: rgb(0, 0, 0),
        size: 14,
      });
    });
    lineStart -= lineIncreasingValue;
  });
  if (comment && comment != '') {
    let xSpace = columSizes[0];
    lineStart -= 20;
    let newcomment = '';
    for (let i = 0; i < comment.length; i++) { 
      newcomment += comment[i];
      if (i % 60 == 0 && i != 0) { 
        newcomment += '\n';
      }
    }
    page.drawText(translate('comment', languageCode), {
      x: xSpace,
      y: lineStart,
      font: fonts.montserratBoldFont,
      color: rgb(0, 0, 0),
      size: 14,
    });
    lineStart -= 40;
    page.drawText(newcomment, {
      x: xSpace,
      y: lineStart,
      font: fonts.montserratFont,
      color: rgb(0, 0, 0),
      size: 14,
    });
  }
};

const drawIssueTo = (
  page: PDFPage,
  fonts: PDFFontObject,
  invoice: InvocieStateProps,
  languageCode: string
) => {
  const {height} = page.getSize();

  const startHeight = height / 1.5;

  page.drawText(`${translate('issueTo', languageCode).toUpperCase()}:`, {
    x: 100,
    y: startHeight,
    font: fonts.antonRegularFont,
    color: rgb(0, 0, 0),
    size: 22,
  });

  page.drawText(`${translate('name', languageCode)}: ${invoice.clientName}`, {
    x: 100,
    y: startHeight - 30,
    font: fonts.montserratRegularFont,
    color: rgb(0, 0, 0),
    size: 18,
  });

  page.drawText(`${translate('clientAddress', languageCode)}: ${invoice.clinetAddress}`, {
    x: 100,
    y: startHeight - 60,
    font: fonts.montserratRegularFont,
    color: rgb(0, 0, 0),
    size: 18,
  });
};
