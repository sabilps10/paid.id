import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import SettingsScreen from '../SettingsScreen';
import CustomText from '../../../components/ui/CustomText';
import {TouchableOpacity, StyleSheet} from 'react-native';
import {useClearStore} from '../../../stores/useClearStore';
import PrivacyPolicyScreen from '../PrivacyPolicyScreen';
import { useNavigation } from '@react-navigation/native';
const Stack = createStackNavigator();

export const SettingsNavigation = () => {
  const [clearAll] = useClearStore();
  const { navigate } = useNavigation();
  return (
    <Stack.Navigator screenOptions={{headerTitleAlign: 'center'}}>
      <Stack.Screen
        name="SettingScreen"
        options={{
          headerRight: () => (
            <TouchableOpacity
              onPress={() => {
                clearAll();
                
                navigate('Home');
              }}
              style={styles.rightText}>
              <CustomText>Logout</CustomText>
            </TouchableOpacity>
          ),
          title: 'Settings',
        }}
        component={SettingsScreen}
      />
      <Stack.Screen
        name="PrivacyPolicyScreen"
        options={{
          title: 'Privacy policy',
        }}
        component={PrivacyPolicyScreen}
      />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  rightText: {
    paddingRight: 24,
  },
});
