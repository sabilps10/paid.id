import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {Button} from 'react-native-material-ui';
import {WebView} from 'react-native-webview';
import {useObserver} from 'mobx-react';
import {BaseScreenView} from '../../components/ui/BaseScreenView';
import {ScrollView} from 'react-native-gesture-handler';

import {WindowDimensions} from '../../utils/WindowDimensions';
import {Loader} from '../../components/ui/Loader';

interface LoadingStateProps {
  loading: boolean;
}

export default () => {
  const [loadingState, setLoadingState] = useState<LoadingStateProps>({
    loading: true,
  });
  return useObserver(() => (
    <BaseScreenView safeAreaStyle={styles.safeAreaContainer}>
      <ScrollView contentContainerStyle={styles.container}>
        {loadingState.loading && <Loader />}
        <WebView
          onLoadEnd={() => setLoadingState({loading: false})}
          source={{uri: 'https://paidapp.id/privacy_policy/'}}
        />
      </ScrollView>
    </BaseScreenView>
  ));
};

const styles = StyleSheet.create({
  safeAreaContainer: {
    flex: 1,
    backgroundColor: '#F9F9F9',
  },
  container: {
    flex: 1,
  },
});
