import React, {useState} from 'react';
import {View, StyleSheet, Alert} from 'react-native';
import {TextField} from 'react-native-material-textfield';
import {Title} from '../../components/ui/Title';
import {Button} from 'react-native-material-ui';
import {BaseScreenView} from '../../components/ui/BaseScreenView';
import {useStores} from '../../stores';
import {useObserver} from 'mobx-react';
import {useUpdateUserDataMutation} from '../../graphql';
import {useTranslate} from '../../hooks/useTranslate';
import {ScrollView} from 'react-native-gesture-handler';
import {HomeChooseLanguage} from '../../components/ui/HomeChooseLanguage';
import {ReimportContacts} from '../../components/ui/ReimportContacts';
import {Impressum} from '../../components/ui/Impressum';
import { CustomInput } from '../../components/ui/CustomInput';

export type SettingsError = 'OK' | 'EMAILERROR' | 'COMPANYNAMEERROR';
const EMAILERROR: SettingsError = 'EMAILERROR';
const COMPANYNAMEERROR: SettingsError = 'COMPANYNAMEERROR';

export interface SettingsErrorStateProps {
  settingsErrors: SettingsError[];
}

export interface SettingState {
  companyName: string;
  companyAddress: string;
  companyNumber: string;
  companyWebsite: string;
  companyEmailAdress: string;
}

export default () => {
  const { userStore, contextStore } = useStores();
  const [translate] = useTranslate();

  const [settingState, setSettingState] = useState<SettingState>({
    companyName: userStore.companyName || '',
    companyAddress: userStore.companyAddress,
    companyNumber: userStore.companyNumber,
    companyWebsite: userStore.companyWebsite,
    companyEmailAdress: userStore.companyEmailAdress,
  });

  const [settingsErrorsState, setSettingsErrorsState] = useState<
    SettingsErrorStateProps
  >({
    settingsErrors: [],
  });

  const [updateUserDataMutation] = useUpdateUserDataMutation({
    variables: {
      data: {
        companyName: settingState.companyName,
        companyAddress: settingState.companyAddress,
        companyNumber: settingState.companyNumber,
        companyWebsite: settingState.companyWebsite,
        companyEmailAdress: settingState.companyEmailAdress,
      },
    },
  });

  const saveUserData = () => {
    userStore.savePublicData({
      companyName: settingState.companyName,
      companyAddress: settingState.companyAddress,
      companyNumber: settingState.companyNumber,
      companyWebsite: settingState.companyWebsite,
      companyEmailAdress: settingState.companyEmailAdress,
    });
    updateUserDataMutation();
    Alert.alert('Saved...');
  };
  const validateInput = (): SettingsError[] => {
    let errors: SettingsError[] = [];
    if (settingState.companyName.length < 5) {
      errors.push(COMPANYNAMEERROR);
    }
    if (
      settingState.companyEmailAdress.length != 0 &&
      settingState.companyEmailAdress.match(/.+@.+\..+/) == null
    ) {
      errors.push(EMAILERROR);
    }
    return errors;
  };

  return useObserver(() => (
    <BaseScreenView safeAreaStyle={styles.upContainer}>
      <ScrollView style={styles.container}>
        <Title
          title={translate('settings')}
          fontStyle={styles.titleStyle}
          containerStyle={styles.titleContainer}
        />
        <HomeChooseLanguage />
        <ReimportContacts />
        <CustomInput
          value={settingState.companyName}
          label={translate('companyName')}
          onInputChange={
            text =>
            setSettingState({...settingState, companyName: text})
          }
          error={
            settingsErrorsState.settingsErrors.find(
              elem => elem === COMPANYNAMEERROR,
            )
              ? translate('companyNameError')
              : undefined
          }
        />
        <CustomInput
          value={settingState.companyAddress}
          label={translate('companyAddress')}
          onInputChange={
            text =>
            setSettingState({...settingState, companyAddress: text})
          }
        />
        <CustomInput
          label={translate('phoneNumber')}
          keyboardType="phone-pad"
          value={settingState.companyNumber}
          onInputChange={
            text =>
            setSettingState({...settingState, companyNumber: text})
          }
        />
        <CustomInput
          value={settingState.companyWebsite}
          label={translate('website')}
          autoCapitalize={'none'}
          keyboardType="url"
          onInputChange={
            text =>
            setSettingState({...settingState, companyWebsite: text})
          }
        />
        <CustomInput
          value={settingState.companyEmailAdress}
          label={translate('publicEmail')}
          autoCapitalize={'none'}
          keyboardType="email-address"
          onInputChange={
            text =>
            setSettingState({...settingState, companyEmailAdress: text})
          }
          error={
            settingsErrorsState.settingsErrors.find(elem => elem === EMAILERROR)
              ? translate('invalidEmail')
              : undefined
          }
        />
        <Button
          upperCase={false}
          style={{
            container: styles.regButtonStyle,
            text: styles.buttonTextStyle,
          }}
          primary
          text={translate('save')}
          onPress={() => {
            let errors: SettingsError[] = validateInput();
            if (errors.length != 0) {
              setSettingsErrorsState({
                settingsErrors: errors,
              });
            } else {
              setSettingsErrorsState({
                settingsErrors: [],
              });
              saveUserData();
            }
          }}
        />
        <Impressum />
        <View style={{height: 100}} />
      </ScrollView>
    </BaseScreenView>
  ));
  // I don't really know why this works, but something is not okay in the scrollview height calculation. I found this 100 pixel high view solution on stackoverflow
};

const styles = StyleSheet.create({
  upContainer: {
    flex: 1,
    backgroundColor: '#fff',
  },
  container: {
    //also this is from the same stackoverflow question, they say in scrollviews it cannot make the calculation if flex: 1 is before the padding.
    padding: 24,
    flex: 1,
  },
  regButtonStyle: {
    borderWidth: 1,
    borderColor: '#E42320',
    minHeight: 55,
    marginTop: 24,
  },
  buttonStyle: {
    borderWidth: 1,
    borderColor: '#E42320',
    minHeight: 55,
  },
  buttonTextStyle: {
    fontSize: 22,
  },
  titleStyle: {
    color: '#000',
    fontWeight: '800',
    fontSize: 22,
  },
  titleContainer: {
    marginBottom: 12,
  },
});
