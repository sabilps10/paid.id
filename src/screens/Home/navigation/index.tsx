import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '../HomeScreen';
import {useStores} from '../../../stores';
import {useObserver} from 'mobx-react';
const Stack = createStackNavigator();

export const HomeNavigation = () => {
  const {userStore} = useStores();
  return useObserver(() => (
    <Stack.Navigator screenOptions={{headerTitleAlign: 'center'}}>
      <Stack.Screen
        name="HomeScreen"
        options={{
          title: userStore.companyName,
        }}
        component={HomeScreen}
      />
    </Stack.Navigator>
  ));
};
