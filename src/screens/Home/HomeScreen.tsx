import React, {useEffect} from 'react';
import {StyleSheet, Button} from 'react-native';
import {RoundContactList} from '../../components/ui/RoundContactList';
import {MemberStatus} from '../../components/ui/MemberStatus';
import {useObserver} from 'mobx-react';
import {BaseScreenView} from '../../components/ui/BaseScreenView';
import {ScrollView} from 'react-native-gesture-handler';
import {useMe} from '../../hooks/useMe';
import {HomeLinks} from '../../components/ui/HomeLinks';
import { useStores } from '../../stores';
import { HomeChooseLanguage } from '../../components/ui/HomeChooseLanguage';
import { RecentsList } from '../../components/ui/RecentsList';
import { HomeBottomLinks } from '../../components/ui/HomeBottomLinks';
import Modals from '../../components/ui/Modals';

export default () => {
  return useObserver(() => (
    <BaseScreenView safeAreaStyle={styles.safeAreaContainer}>
      <ScrollView contentContainerStyle={styles.container}>
        <MemberStatus />
        <RoundContactList />
        <RecentsList />
        <HomeLinks />
        <HomeBottomLinks />
        <Modals />
      </ScrollView>
    </BaseScreenView>
  ));
};

const styles = StyleSheet.create({
  safeAreaContainer: {
    flex: 1,
    backgroundColor: '#F9F9F9',
  },
  container: {},
});
