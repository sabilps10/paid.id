import React, { useEffect, useState } from 'react';
import {BaseScreenView} from '../../components/ui/BaseScreenView';
import {View, StyleSheet, Modal } from 'react-native';
import {useStores} from '../../stores';
import {PDFViewer} from '../../voucherkit/PDFViewer';
import { Button } from 'react-native-material-ui';
import { WindowDimensions } from '../../utils/WindowDimensions';
import Share from 'react-native-share';
import RNFS from 'react-native-fs';

import RNFetchBlob from 'rn-fetch-blob';
import { useTranslate } from '../../hooks/useTranslate';
import CustomText from '../../components/ui/CustomText';
import LocalizedText from '../../components/ui/LocalizedText';

export interface InvoicePDFViewProps {
  route: any
}

export const InvoicePDFView = (props) => {
  const { localInvoiceId, close } = props;

  const { invoiceStore, userStore, contextStore } = useStores();
  const [translate] = useTranslate();

  const invoice = invoiceStore.invoice.find(
    inv => inv.localInvoiceId === localInvoiceId,
  );
  const share = async () => {
    const newPath = `invoice-${RNFetchBlob.fs.dirs.CacheDir}/${invoice?.invoiceNumber}.pdf`;
    if (invoice === undefined)
      return;
    console.log(invoice);
    let data;
    if (invoice.invoiceUrlPath) {
      data = await (await RNFetchBlob.fetch('get', invoice.invoiceUrlPath)).base64();
    } else { 
      data = await RNFetchBlob.fs.readFile(invoice.invoiceLocalPath, 'base64');
      console.log('url path problem');
    }
    const shareOptions = {
      title: 'Share your PA.ID invoice!',
      subject: `Invoice - ${invoice?.invoiceNumber}`,
      message: `Dear ${invoice?.clientName}!\n\n${userStore.companyName} ${translate('shareText')} ${(new Date(invoice.payTerm)).toLocaleDateString()}.\n\n${translate('invoiceNumber')}: ${invoice.invoiceNumber}\n${translate('sentByPaid')}\n${translate('paidUrl')}`,
      email: invoice.clientEmail || '',
      url: `data:application/pdf;base64,${data}`,
      type: 'application/pdf',
      failOnCancel: true,
    };
    Share.open(shareOptions)
    .then((res) => { console.log(res) })
    .catch((err) => { err && console.log('share error: ', err); });
  }
  console.log('invoice--1', invoice);

  return (
    <Modal
    animationType="slide"
      visible={true}
      onRequestClose={() => close()}>
      <BaseScreenView safeAreaStyle={styles.safeAreaStyle}>
        <View style={styles.shareButtonContainer}>
          <Button
            upperCase={false}
            style={{
              container: styles.regButtonStyle,
              text: styles.buttonTextStyle,
            }}
            onPress={() => {
              console.log('close modal');
              close();
            }}
            text={translate('back')}
          />
          <Button
              upperCase={false}
              onPress={() => share()}
              style={{
                container: styles.regButtonStyle,
                text: styles.buttonTextStyle,
              }}
              text="Share"
            />
        </View>
      <View style={{flex: 1, justifyContent: "center"}}>
        {invoice && invoice.localInvoiceId && <PDFViewer source={{uri: invoice?.invoiceUrlPath || invoice?.invoiceLocalPath}} />}
        </View>
      </BaseScreenView>
      </Modal>
  );
};

const styles = StyleSheet.create({
  safeAreaStyle: {
    flex: 1,
    justifyContent: 'center',
  },
  shareButtonContainer: {
    paddingTop: 12,
    margin: WindowDimensions.width025,
  },
  box: {
    backgroundColor: '#fff',
    padding: 12,
    marginBottom: 12,
  },
  regButtonStyle: {
    borderWidth: 1,
    borderColor: '#000',
    minHeight: 42,
    marginBottom: 12,
  },
  buttonStyle: {
    borderWidth: 1,
    borderColor: '#000',
    minHeight: 42,
  },
  buttonTextStyle: {
    fontSize: 18,
  },
});
