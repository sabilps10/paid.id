import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  FlatList,
  TouchableHighlight,
  Alert, TouchableOpacity
} from 'react-native';
import {Invoice} from 'src/types/Invoice.types';
import moment from 'moment';
import {BaseScreenView} from '../../components/ui/BaseScreenView';
import {useObserver} from 'mobx-react';
import {useStores} from '../../stores';
import CustomText from '../../components/ui/CustomText';
import {useNavigation} from '@react-navigation/native';
import { useDeleteInvoiceMutation } from '../../graphql';
import { OpenModal } from '../../stores/ContextStore';
import { calculateTotal, formatAmount } from '../../utils/Amount'

export const InvoiceListScreen = () => {
  const {invoiceStore, contextStore} = useStores();
  const { navigate } = useNavigation();
  const [deleteMutation] = useDeleteInvoiceMutation();

  return useObserver(() => (
    <BaseScreenView safeAreaStyle={styles.upContainer}>
      <View style={styles.container}>
        {invoiceStore.invoice.length === 0 && (
          <CustomText style={{textAlign: 'center', fontSize: 16}}>
            You haven't created any invoces yet {'\n'}
            Press the left bar "New Invoice" button
          </CustomText>
        )}
        <FlatList
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          data={invoiceStore.invoice.slice().reverse()}
          keyExtractor={() => Math.random().toString()}
          renderItem={({item}) => (
            <TouchableOpacity
              activeOpacity={.6}
              onPress={() => {
                console.log(item.total);
                contextStore.openModal(OpenModal.PDFVIEW, item.localInvoiceId);
              }}
              onLongPress={() => { 
                Alert.alert(
                  "Delete Invoice",
                  `Do you really wish to delete invoice ${item.invoiceNumber}?`,
                  [
                    {
                      text: "Cancel",
                      onPress: () => {},
                      style: "cancel"
                    },
                    {
                      text: "Yes", onPress: () => { 
                        deleteMutation({
                          variables: {
                            invoiceNumber: item.invoiceNumber
                          }
                        });
                        invoiceStore.deleteInvoice(item.invoiceNumber);
                    } }
                  ],
                  { cancelable: false }
                );
              }}
              style={styles.box}>
              <View>
                <View style={[styles.boxContainer, styles.margin]}>
                  <Text style={styles.boxText}>{item.invoiceNumber}</Text>
                  <Text style={styles.boxText}>
                    {moment(item.payTerm).format('DD/MM/YYYY')}
                  </Text>
                </View>
                <View style={styles.boxContainer}>
                  <Text style={styles.boxText}>{item.clientName}</Text>
                  <Text style={styles.boxText}>{formatAmount(calculateTotal(item))}</Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    </BaseScreenView>
  ));
};

const styles = StyleSheet.create({
  upContainer: {
    flex: 1,
    backgroundColor: '#fff',
  },
  container: {
    flex: 1,
    padding: 24,
    backgroundColor: 'rgba(0,0,0,0.09)',
  },
  box: {
    backgroundColor: '#fff',
    padding: 12,
    marginBottom: 12,
  },
  boxContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  margin: {
    marginBottom: 12,
  },
  boxText: {
    fontSize: 16,
  },
});