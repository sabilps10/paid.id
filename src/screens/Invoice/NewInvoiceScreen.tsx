import React, {useState, useEffect, Ref, LegacyRef} from 'react';
import {View, StyleSheet, Text, Keyboard, Modal, TouchableOpacity} from 'react-native';
import {TextField} from 'react-native-material-textfield';
import {Button} from 'react-native-material-ui';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker';
import {WindowDimensions} from '../../utils/WindowDimensions';
import {BaseScreenView} from '../../components/ui/BaseScreenView';
import {useObserver} from 'mobx-react';
import {useStores} from '../../stores';
import {usePDFKit} from '../../voucherkit/usePDFKit';
import reduce from 'lodash/reduce';
import md5 from 'md5';
import ProductList from '../../components/ui/ProductList';
import { useInvoiceNumberQuery, useCreateInvoiceMutation } from '../../graphql';
import LocalizedText from '../../components/ui/LocalizedText';
import { useTranslate } from '../../hooks/useTranslate';
import { readFile } from 'react-native-fs';
import { Loader } from '../../components/ui/Loader';
import CustomText from '../../components/ui/CustomText';
import { OpenModal } from '../../stores/ContextStore';
import { CustomInput } from '../../components/ui/CustomInput'
import { formatAmount } from '../../utils/Amount';

export type InvoiceError = 'OK' | 'CLIENTERROR' | 'CLIENTEMAILERROR' | 'PRODUCTPARAMETERERROR' | 'DISCOUNTERROR' | 'SHIPPINGERROR' | 'TAXERROR';
const OK: InvoiceError = 'OK';
const CLIENTERROR: InvoiceError = 'CLIENTERROR';
const CLIENTEMAILERROR: InvoiceError = 'CLIENTEMAILERROR';
const PRODUCTPARAMETERERROR: InvoiceError = 'PRODUCTPARAMETERERROR';
const DISCOUNTERROR: InvoiceError = 'DISCOUNTERROR';
const SHIPPINGERROR: InvoiceError = 'SHIPPINGERROR';
const TAXERROR: InvoiceError = 'TAXERROR';

export interface Product {
  name: string;
  quantity: number;
  price: number;
}

export interface InvoiceErrorStateProps {
  invoiceErrors: InvoiceError[]
};

export interface InvocieStateProps {
  localInvoiceId: string;
  clientName: string;
  clinetAddress: string;
  clientEmail: string;
  invoiceNumber: string;
  payTerm: Date;
  products: Array<Product>;
  taxPercent: number;
  discountPercent: number;
  shipping: number;
  comment: string;
  invoiceLocalPath: string;
}

export interface InvoiceAmountProps {
  subTotal: number;
  tax: number;
  total: number;
}
enum UploadState { 
  INIT,
  LOADING,
  UPLOAD
}
const INIT = UploadState.INIT;
const LOADING = UploadState.LOADING;
const UPLOAD = UploadState.UPLOAD;

export interface InvoiceUploadState {
  state: UploadState;
  source?: string;
}

export default (props) => {
  const { userStore, invoiceStore, contextStore } = useStores();
  const [translate] = useTranslate();
  const { create, pdfkitState, resetPDFState } = usePDFKit();
  const invoiceNumber = useInvoiceNumberQuery();

  const { close, contact } = props;
  const [invoiceUploadState, setInvoiceUploadState] = useState<InvoiceUploadState>({
    state: INIT,
  });

  const [invoiceState, setInvoiceState] = useState<InvocieStateProps>({
    localInvoiceId: md5(Math.random().toString()),
    clientName: contact ? [contact.firstName, contact.lastName].join(' ') : '',
    clinetAddress: contact ? contact.companyAddress : '',
    clientEmail: contact ? contact.email : '',
    invoiceNumber: '',
    payTerm: new Date(),
    products: [
      {
        name: '',
        quantity: 1,
        price: 10,
      },
    ],
    taxPercent: 0,
    discountPercent: 0,
    shipping: 0,
    comment: '',
    invoiceLocalPath: '',
  });
  const [invoiceErrorsState, setInvoiceErrorsState] = useState<InvoiceErrorStateProps>({
    invoiceErrors: []
  });
  if (invoiceNumber.data && invoiceState.invoiceNumber == '') {
    console.log("data: ", invoiceNumber.data)
    setInvoiceState({
      ...invoiceState,
      invoiceNumber: invoiceNumber.data.invoiceNumber.invoiceNumber || '0'
    });
  }
  const [amountState, setAmountState] = useState<InvoiceAmountProps>({
    tax: 0,
    total: 0,
    subTotal: 0,
  });
  //upload
  const createInvoice = async () => {
    setInvoiceUploadState({...invoiceUploadState, state: LOADING});
    invoiceStore.saveInvoice({ ...invoiceState, ...amountState});
    await create(
      invoiceState,
      {
        companyName: userStore.companyName || '',
        companyEmail: userStore.companyEmailAdress,
        companyNumber: userStore.companyNumber,
      },
      {
        subTotal: Math.round(amountState.subTotal),
        tax: Math.round(amountState.tax),
        total: Math.round(amountState.total),
      },
      contextStore.languageCode
    ).catch(() => setInvoiceUploadState({...invoiceUploadState, state: INIT}));
  };
  if (pdfkitState.pdfHasFinished && invoiceUploadState.state == LOADING) { 
    invoiceStore.updateInvoiceLocalPath(
      invoiceState.localInvoiceId,
      `${pdfkitState.source}` || '',
    );
     readFile(pdfkitState.source, 'base64').then(source => {
      setInvoiceUploadState({
        state: UPLOAD,
        source: source,
      });
    }).catch(() => setInvoiceUploadState({...invoiceUploadState, state: INIT}));
  }
  const [uploadMutation] = useCreateInvoiceMutation({
    variables: {
      data: { ...invoiceState },
      pdf: `${invoiceUploadState.source}` || '',
    },
  });
  useEffect(() => {
    if (invoiceUploadState.state == UPLOAD) {
      uploadMutation().then(() => { 
        setInvoiceUploadState({...invoiceUploadState, state: INIT});
        resetPDFState();
        contextStore.openModal(OpenModal.PDFVIEW, invoiceState?.localInvoiceId);
      }).catch(error => {
        console.log('mutation error: ', error);
        setInvoiceUploadState({...invoiceUploadState, state: INIT});
      });

    }
    return () => {};
  }, [
    invoiceState,
    invoiceUploadState,
    invoiceUploadState.state,
    uploadMutation
  ]);
  // upload pdf
  const handleChangeProduct = async (
    key: number,
    text: string,
    field: string,
    toFloat: boolean,
  ) => {
    let {products} = invoiceState;

    console.log('products', products);

    let selectedProduct = products[key];
    selectedProduct[field] = toFloat ? text.replace(/[^0-9]/g, '') == '' ? 0 :parseFloat(text.replace(/[^0-9]/g, '')) : text;
    products[key] = selectedProduct;
    console.log('selectedProduct', selectedProduct);

    setInvoiceState({
      ...invoiceState,
      products,
    });
  };

  useEffect(() => {
    const {products, taxPercent, discountPercent, shipping} = invoiceState;
    let fullProdPrices = reduce(
      products,
      (prev, curr) => {
        return prev + curr.price * curr.quantity;
      },
      0,
    );
    let reducedWithDiscount = fullProdPrices;
    if (discountPercent > 0) {
      reducedWithDiscount -= fullProdPrices * discountPercent / 100;
    }
    let taxedPrice = reducedWithDiscount;
    if (taxPercent > 0) {
      taxedPrice += reducedWithDiscount * taxPercent / 100;
    }
    let total = taxedPrice + shipping;
    setAmountState({
      subTotal: fullProdPrices,
      tax: taxedPrice - reducedWithDiscount,
      total: total,
    });
  }, [invoiceState]);

  const validateInput = (): InvoiceError[] => {
    let errors: InvoiceError[] = [];
    if (invoiceState.clientName.length < 5) {
      errors.push(CLIENTERROR);
    }
    if (invoiceState.clientEmail.length != 0 && invoiceState.clientEmail.match(/.+@.+\..+/) == null) {
      errors.push(CLIENTEMAILERROR);
    }
    if (invoiceState.taxPercent < 0 || invoiceState.taxPercent >= 100) { 
      errors.push(TAXERROR);
    }
    if (invoiceState.discountPercent < -100 && invoiceState.discountPercent > 100) {
      errors.push(DISCOUNTERROR);
    }
    if (invoiceState.shipping < 0) {
      errors.push(SHIPPINGERROR);
    }
    if (invoiceState.products.find(elem => elem.name.length < 5)){ 
      errors.push(PRODUCTPARAMETERERROR);
    }
    return errors;
  };

  let clientNameInput = React.createRef<TextInput>();
  let clientEmailInput = React.createRef<TextInput>();
  let productsInput = React.createRef<TextInput>();
  let taxInput = React.createRef<TextInput>();
  let discountInput = React.createRef<TextInput>();
  let shippingInput = React.createRef<TextInput>();
  let datePicker;

  return useObserver(() => (
    <Modal
      animationType="slide"
      transparent={true}
      visible={true}
      onRequestClose={() => close()}
    >
    <BaseScreenView safeAreaStyle={styles.upContainer}>
        {invoiceUploadState.state != INIT && <Loader />}
        <View style={styles.container}>
          <View style={[styles.box, {flexDirection:'row'}]}>
              
              <View style={{ flex: 1, alignContent: 'center' }}>
                <CustomText textType="regular" style={{fontSize:16, fontWeight:'900',padding: 12, textAlign: 'center'}}>
                  <LocalizedText content="createInvoice"></LocalizedText>
                </CustomText>
              </View>
          </View>
          <Button
              style={{
                container: {
                  top: 16,
                  left: 16,
                  position: 'absolute'
                }
              }}
              onPress={() => {
                console.log('close modal');
                close();
              }}
              text={translate('back')}
            />
        <ScrollView contentContainerStyle={styles.scrollContainer}>
          <View style={styles.box}>
            <Text style={styles.boxTitle}>{userStore.companyName}</Text>
            <Text style={styles.boxDescription}>
              {userStore.companyAddress}
              {'\n'}
              {userStore.companyEmailAdress}
            </Text>
          </View>
          <View style={styles.box}>
            <Text style={styles.boxTitle}>
              <LocalizedText content="client"/>
            </Text>
            <CustomInput
              label={translate('clientName')}
              keyboardType="default"
              value={invoiceState.clientName}
              onInputChange={text =>
                setInvoiceState({
                  ...invoiceState,
                  clientName: text,
                })
              }
              ref={(inp) => clientNameInput = inp }
              error={
                invoiceErrorsState.invoiceErrors.find(elem => elem === CLIENTERROR) ? translate('clientNameError') : undefined
              }
            />
            <CustomInput
              label={translate('clientAddress')}
              keyboardType="default"
              value={invoiceState.clinetAddress}
              onInputChange={text =>
                setInvoiceState({
                  ...invoiceState,
                  clinetAddress: text,
                })
              }
            />
            <CustomInput
              label={translate('clientEmail')}
              autoCapitalize={'none'}
              keyboardType="email-address"
              value={invoiceState.clientEmail}
              onInputChange={text =>
                setInvoiceState({
                  ...invoiceState,
                  clientEmail: text,
                })
              }
              ref={(input) =>  clientEmailInput = input}
              error={
                invoiceErrorsState.invoiceErrors.find(elem => elem === CLIENTEMAILERROR) ? translate('invalidEmail') : undefined
              }
            />
          </View>
          <View style={styles.box}>
            <Text style={styles.boxTitle}>Invoice details</Text>
            <View style={styles.detailsContaienr}>
              <CustomInput
                label={translate('invoiceNumber')}
                disabled={true}
                containerStyle={{width: WindowDimensions.width40}}
                keyboardType="default"
                value={invoiceState.invoiceNumber}
                onInputChange={text =>
                  setInvoiceState({
                    ...invoiceState,
                    invoiceNumber: text,
                  })
                }
              />
               <DatePicker
                style={{ display: 'none' }}
                date={invoiceState.payTerm}
                ref={(picker) => { datePicker = picker; }}
                mode="date"
                placeholder="select date"
                format="YYYY-MM-DD"
                minDate={new Date()}
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0
                  },
                  dateInput: {
                    marginLeft: 36
                  }
                }}
                onDateChange={date => {
                  const d = new Date(`${date}`);
                  d.setMinutes(d.getMinutes() + d.getTimezoneOffset());
                  setInvoiceState({
                    ...invoiceState,
                    payTerm: d,
                  });
                }
                }
              />
                <TouchableOpacity
                  onPress={() => {
                    datePicker.onPressDate();
                  }}
                  style={{flex:1, marginLeft: 24}}
              >
                <CustomInput
                  disabled={true}
                  label={translate('term')}
                  containerStyle={{ width: WindowDimensions.width40 }}
                  keyboardType="default"
                  value={invoiceState.payTerm.toDateString()}
                  />
                </TouchableOpacity>
              
            </View>
          </View>
          <ProductList products={invoiceState.products} handleChangeProduct={handleChangeProduct} errorFocus={(input) => { productsInput = input; }} />
          <Button
            upperCase={false}
            onPress={() => {
              setInvoiceState({
                ...invoiceState,
                products: [
                  ...invoiceState.products,
                  { name: '', quantity: 1, price: 10 },
                ],
              });
            }}
            style={{
              container: styles.regButtonStyle,
              text: styles.buttonTextStyle,
            }}
            text={translate('addProduct')}
          />
          <View style={styles.box}>
            <Text style={styles.boxTitle}>
              <LocalizedText content="details"/>
            </Text>
            <View style={styles.detailsContaienr}>
              <CustomInput
                label={translate('tax')}
                containerStyle={{ width: WindowDimensions.width40 }}
                keyboardType="numeric"
                value={invoiceState.taxPercent == 0 ? '':invoiceState.taxPercent.toString()}
                onInputChange={text =>
                  setInvoiceState({
                    ...invoiceState,
                    taxPercent: text.replace(/[^0-9]/g, '') == '' ? 0 :parseInt(text.replace(/[^0-9]/g, '')),
                  })
                }
                error={invoiceErrorsState.invoiceErrors.find(elem => elem === TAXERROR) ? translate('taxError') : undefined}
                ref={(input) => taxInput = input}
              />
              <CustomInput
                label={translate('discount')}
                containerStyle={{ width: WindowDimensions.width40 }}
                keyboardType="numeric"
                value={invoiceState.discountPercent == 0 ? '':invoiceState.discountPercent.toString()}
                onInputChange={event => {
                  setInvoiceState({
                    ...invoiceState,
                    discountPercent: event.replace(/[^0-9]/g, '') == '' ? 0 :parseInt(event.replace(/[^0-9]/g, '')),
                  });
                }  
                }
                error={invoiceErrorsState.invoiceErrors.find(elem => elem === DISCOUNTERROR) ? translate('discountError') : undefined}
                ref={(input) => discountInput = input}

              />
            </View>
            <CustomInput
                label={translate('shipping')}
                keyboardType="numeric"
                value={invoiceState.shipping == 0 ? '' : invoiceState.shipping.toString()}
                onInputChange={event => {
                  setInvoiceState({
                    ...invoiceState,
                    shipping: event.replace(/[^0-9]/g, '') == '' ? 0 : parseInt(event.replace(/[^0-9]/g, '')),
                  });
                }
                }
                ref={(input) => shippingInput = input}
                error={invoiceErrorsState.invoiceErrors.find(elem => elem === SHIPPINGERROR) ? translate('shippingError') : undefined}
              />
            <CustomInput
              label={translate('comment')}
              keyboardType="default"
              value={invoiceState.comment}
              onInputChange={text =>
                setInvoiceState({
                  ...invoiceState,
                  comment: text,
                })
              }
            />
          </View>
        </ScrollView>
        <View style={styles.bottomContainer}>
          <Text style={styles.amountSize}>{formatAmount(Math.round(amountState.total))}</Text>
          <Button
            onPress={() => {
              let errors: InvoiceError[] = validateInput();
              if (errors.length != 0) {
                setInvoiceErrorsState({
                  invoiceErrors: errors
                });
                Keyboard.dismiss();

                if (errors[0] == CLIENTERROR) {
                  clientNameInput.focus();
                }
                else if (errors[0] == CLIENTEMAILERROR) {
                  clientEmailInput.focus();
                }
                else if (errors[0] == TAXERROR) { 
                  taxInput.focus();
                }
                else if (errors[0] == DISCOUNTERROR) {
                  discountInput.focus();
                }
                else if (errors[0] == SHIPPINGERROR) {
                  shippingInput.focus();
                }
                else if (errors[0] == PRODUCTPARAMETERERROR) {
                  productsInput.focus();
                }
              } else {
                setInvoiceErrorsState({
                  invoiceErrors: []
                });
                createInvoice()
              }
            }}
            upperCase={false}
            style={{
              container: [
                styles.regButtonStyle,
                { width: WindowDimensions.width45 },
              ],
              text: styles.buttonTextStyle,
            }}
            text={translate('save')}
          />
        </View>
      </View>
      </BaseScreenView>
      </Modal>
  ));
};

const styles = StyleSheet.create({
  upContainer: {
    flex: 1,
    backgroundColor: '#fff',
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.08)',
  },
  box: {
    backgroundColor: '#fff',
    padding: 12,
    marginBottom: 12,
  },
  boxTitle: {
    fontSize: 16,
    fontWeight: '800',
    paddingBottom: 4,
  },
  boxDescription: {
    fontSize: 14,
  },
  scrollContainer: {
    paddingTop: 12,
    paddingLeft: 12,
    paddingRight: 12,
    paddingBottom: 33,
  },
  detailsContaienr: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  regButtonStyle: {
    borderWidth: 1,
    borderColor: '#000',
    minHeight: 42,
    marginBottom: 12,
  },
  buttonStyle: {
    borderWidth: 1,
    borderColor: '#000',
    minHeight: 42,
  },
  buttonTextStyle: {
    fontSize: 18,
  },
  amountSize: {
    fontSize: 22,
  },
  bottomContainer: {
    bottom: 0,
    left: 0,
    right: 0,
    padding: 12,
    backgroundColor: '#fff',
    borderTopColor: '#515151',
    borderTopWidth: 1,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
