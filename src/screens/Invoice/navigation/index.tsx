import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import NewInvoiceScreen from '../NewInvoiceScreen';
import {Text, TouchableOpacity} from 'react-native';
import {InvoiceListScreen} from '../InvoiceListScreen';
import CustomText from '../../../components/ui/CustomText';
import {InvoicePDFView} from '../InvoicePDFView';
import {InvoiceUploadScreen} from '../InvoiceUploadScreen';
import { useStores } from '../../../stores';
import { OpenModal } from '../../../stores/ContextStore';
const Stack = createStackNavigator();

export const InvoiceNavigation = ({ navigation }) => {
  const { contextStore } = useStores();
  return (
    <Stack.Navigator screenOptions={{headerTitleAlign: 'center'}}>
      <Stack.Screen
        name="InvoiceList"
        options={{
          headerBackTitle: 'Back',
          title: 'Invoice list',
          headerLeft: () => {
            return (
              <TouchableOpacity
                style={{paddingLeft: 24}}
                onPress={() => contextStore.openModal(OpenModal.NEWINVOICE)}>
                <CustomText style={{fontSize: 16}}>New Invoice</CustomText>
              </TouchableOpacity>
            );
          },
        }}
        component={InvoiceListScreen}
      />
    </Stack.Navigator>
  );
};
