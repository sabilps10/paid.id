import React from 'react';
import ApolloClient from 'apollo-client';
import UserStore from '../stores/UserStore';
import {HttpLink} from 'apollo-link-http';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {onError} from 'apollo-link-error';
import {ApolloLink, concat} from 'apollo-link';
import {buildAxiosFetch} from 'axios-fetch';
import Axios from 'axios';
import {useStores} from '../stores';
import AsyncStorage from '@react-native-community/async-storage';
import {setContext} from 'apollo-link-context';

const axios = Axios.create();

export const useApolloContext = () => {
  const getToken = async () => {
    let response = (await AsyncStorage.getItem('token')) || null;
    console.log('getToken', response);
    return response;
  };

  const asyncAuthLink = setContext(
    () =>
      new Promise(async success => {
        let token = await getToken();
        console.log('getToken2', token);
        success({headers: {authorization: token}});
      }),
  );

  const httplink = new HttpLink({
    uri: 'https://api-a.paidapp.id/graphql',
    credentials: 'include',
    fetch: buildAxiosFetch(axios),
  });

  const getClient = () => {
    return new ApolloClient({
      connectToDevTools: true,
      cache: new InMemoryCache(),
      link: concat(asyncAuthLink, httplink),
    });
  };

  return [getClient];
};
