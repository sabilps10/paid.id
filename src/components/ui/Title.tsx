import React from 'react';
import CustomText from './CustomText';
import {WindowDimensions} from '../../utils/WindowDimensions';
import {View, StyleSheet, ViewStyle, Text} from 'react-native';
import {TextStyle} from 'react-native-material-ui';

export interface IHomeTitle {
  title?: String;
  fontStyle?: TextStyle;
  containerStyle?: ViewStyle;
}

export const Title = ({title, fontStyle, containerStyle}: IHomeTitle) => {
  return (
    <View style={[containerStyle ? containerStyle : styles.container]}>
      <Text style={[fontStyle || styles.fontStyle]}>{title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  fontStyle: {
    fontSize: 38,
    fontWeight: '800',
    color: '#fff',
  },
});
