import React, {PropsWithChildren} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ViewStyle} from 'react-native-material-ui';
import {useObserver, observer} from 'mobx-react';
import {View} from 'react-native';
import {AuthenticationModalBase} from '../auth/AuthenticationBaseModal';
import {useStores} from '../../stores';
import {useMe} from '../../hooks/useMe';
import moment from 'moment';

export interface BaseScreenViewProps {
  safeAreaStyle: ViewStyle;
}

export const BaseScreenView: React.FC<
  PropsWithChildren<BaseScreenViewProps>
> = ({safeAreaStyle, children}) => {
  const {userStore} = useStores();
  const [syncMe] = useMe();

  useObserver(() => {
    if (
      userStore.isLoggedIn === true &&
      typeof userStore.lastSync === 'undefined'
    ) {
      userStore.setLastSyncDate(new Date());
      console.log('syncmee2');
      syncMe();
    }

    if (moment().diff(moment(userStore.lastSync), 'minutes') > 2) {
      userStore.setLastSyncDate(new Date());
      console.log('syncmee55');
      syncMe();
    }
  });

  return useObserver(() => (
    <View style={[safeAreaStyle]}>
      {userStore.isLoggedIn === false && <AuthenticationModalBase />}
      {userStore.isLoggedIn === true && children}
    </View>
  ));
};
