import React from 'react';
import {View, Image, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {WindowDimensions} from '../../utils/WindowDimensions';

interface NoImageProps {
  initials: string;
}

const colors = ['#3aff3a', '#3a3aff', '#ff9d3a', '#3affff', '#ff3a3a'];

const NoImage = (props: NoImageProps) => {
  return (
    <View
      style={[
        styles.container,
        {backgroundColor: colors[Math.floor(Math.random() * 5)]},
      ]}>
      <Text style={styles.text}>
        {props.initials
          .split(' ')
          .slice(0, 2)
          .map(name => name[0])
          .reduce((prev, cur) => prev + cur)
          .toUpperCase()}
      </Text>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    width: WindowDimensions.width / 7,
    height: WindowDimensions.width / 7,
    paddingTop: WindowDimensions.width / 40,
    borderRadius: 100,
    alignContent: 'center',
    textAlignVertical: 'center',
    textAlign: 'center',
    alignItems: 'center',
  },
  text: {
    color: 'white',
    textAlign: 'center',
    fontSize: 28,
  },
});

export default NoImage;
