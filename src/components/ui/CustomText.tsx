import React, {FunctionComponent} from 'react';
import {StyleSheet, Text, TextStyle} from 'react-native';

type CustomTextProps = {
  style?: TextStyle | TextStyle[];
  textType?: 'regular' | 'bold' | 'light' | 'anton';
};

const CustomText: FunctionComponent<CustomTextProps> = ({
  children,
  textType,
  style,
}) => {
  let textStyle: {};
  switch (textType) {
    case 'regular':
      textStyle = styles.regular;
      break;
    case 'bold':
      textStyle = styles.bold;
      break;
    case 'light':
      textStyle = styles.light;
      break;
    default:
      textStyle = styles.regular;
      break;
    case 'anton':
      textStyle = styles.anton;
      break;
  }
  const passedStyles = Array.isArray(style)
    ? Object.assign({}, ...style)
    : style;
  return <Text style={[textStyle, {...passedStyles}]}>{children}</Text>;
};
const styles = StyleSheet.create({
  regular: {
    fontFamily: 'Montserrat-Regular',
  },
  bold: {
    fontFamily: 'Montserrat-Bold',
  },
  light: {
    fontFamily: 'Montserrat-Light',
  },
  anton: {
    fontFamily: 'Anton-Regular',
  },
});
export default CustomText;
