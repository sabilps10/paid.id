import { useObserver } from 'mobx-react';
import React from 'react';
import { View } from 'react-native';
import NewInvoiceScreen from '../../screens/Invoice/NewInvoiceScreen';
import { OpenModal } from '../../stores/ContextStore';
import { useStores} from '../../stores';
import { WriteUsModal } from './WriteUsModal';
import { InvoicePDFView } from '../../screens/Invoice/InvoicePDFView';




const Modals = () => {
  const { contextStore } = useStores();
  
  return useObserver(() => (
    <View style={{opacity:0}}>
      {
        contextStore.modal == OpenModal.NEWINVOICE && <NewInvoiceScreen close={() => contextStore.closeModals()} contact={contextStore.modalData} />
      }{
        contextStore.modal == OpenModal.WRITEUS && <WriteUsModal close={() => contextStore.closeModals()} data={contextStore.modalData} />
      }{
        contextStore.modal == OpenModal.PDFVIEW && <InvoicePDFView close={() => contextStore.closeModals()} localInvoiceId={contextStore.modalData} />
      }
    </View>
  ));
};

export default Modals;
