import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {Button} from 'react-native-material-ui';
import {WindowDimensions} from '../../utils/WindowDimensions';
import {useNavigation} from '@react-navigation/native';
import {useObserver} from 'mobx-react';
import LocalizedText from './LocalizedText';
import CustomText from './CustomText';
import {useContacts} from '../../hooks/useContacts';

export const ReimportContacts: React.FC = () => {
  const [getContacts] = useContacts();
  return useObserver(() => (
    <View style={styles.outerContainer}>
      <View style={styles.innerContainer}>
        <View style={[styles.homeLinkContainer]}>
          <Button
            onPress={() => getContacts()}
            raised
            primary
            text="Reimport Contacts"
          />
        </View>
      </View>
    </View>
  ));
};

const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  innerContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  homeLinkContainer: {
    width: WindowDimensions.width - 48,
  },
  noContactContainer: {
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    paddingTop: 24,
    paddingBottom: 24,
  },
});
