import React, { FunctionComponent, useState } from 'react';
import {View, StyleSheet, Modal} from 'react-native';
import {WindowDimensions} from '../../utils/WindowDimensions';
import {useNavigation} from '@react-navigation/native';
import {useObserver} from 'mobx-react';
import LocalizedText from './LocalizedText';
import CustomText from './CustomText';
import { TextField } from 'react-native-material-textfield';
import { useTranslate } from '../../hooks/useTranslate';
import { useStores } from '../../stores';
import { Button } from 'react-native-material-ui';
import { useCreateIssueMutation } from '../../graphql';

interface IssueData { 
  title?: string;
  message?: string;
}

type CustomTextProps = {
  data?: IssueData
  close(): void
};

interface IssueState { 
  title: string,
  message: string
}

export const WriteUsModal: FunctionComponent<CustomTextProps> = ({
  children,
  data,
  close
}) => {
  const { contextStore } = useStores();
  const [translate] = useTranslate();
  const [createIssueMutation] = useCreateIssueMutation({
    variables: {
      title:  '',
      message:  ''
    },
  });
  const [issueState, setIssueState] = useState<IssueState>({
    title: data && data.title || '',
    message: data && data.message || ''
  });

  return useObserver(() => (
    <Modal
      animationType='fade'
      transparent={true}
      visible={true}
      onRequestClose={() => close() }
    >
      <View style={{
        backgroundColor: 'rgba(151, 151, 151, 0.8)',
        width: WindowDimensions.width,
        height: WindowDimensions.height
      }}>
      <View style={{
        flexDirection: 'column',
        width: WindowDimensions.width / 10 * 8,
        height: WindowDimensions.height / 10 * 6,
        marginLeft: WindowDimensions.width / 10,
        marginTop: WindowDimensions.height / 10,
        backgroundColor: "#fff",
        borderRadius: 10,
        padding: 24,
        paddingTop: 48,
      }}>
      <CustomText
              textType="regular"
              style={{fontSize: 24, fontWeight: '900', textAlign: 'center'}}>
              <LocalizedText content="writeUs" />
            </CustomText>
        <TextField
                tintColor={'#000'}
                baseColor={'#000'}
                textColor={'#000'}
                label={translate('title')}
                labelFontSize={16}
                keyboardType="default"
                value={issueState.title}
                defaultValue={issueState.title}
                onChangeText={text =>
                  setIssueState({
                    ...issueState,
                    title: text,
                  })
                }
        />
        <TextField
                tintColor={'#000'}
                baseColor={'#000'}
                textColor={'#000'}
                label={translate('message')}
                labelFontSize={16}
                keyboardType="default"
                multiline={true}
                value={issueState.message}
                defaultValue={issueState.message}
                onChangeText={text =>
                  setIssueState({
                    ...issueState,
                    message: text,
                  })
                }
        />
        <Button
          upperCase={false}
          style={{
            container: styles.regButtonStyle,
            text: styles.buttonTextStyle,
          }}
          primary
          text={translate('send')}
          onPress={() => {
            createIssueMutation({ variables: { title: issueState.title, message: issueState.message }});
            close();
          }}
          />
          <Button
          upperCase={false}
          style={{
            container: styles.regButtonStyle,
            text: styles.buttonTextStyle,
          }}
          primary
          text={translate('cancel')}
          onPress={() => {
            close();
          }}
        />
      </View>
      </View>
    </Modal>
           
  ));
};

{/* <CustomText
textType="regular"
style={{fontSize: 24, fontWeight: '900', textAlign: 'center'}}>
<LocalizedText content="version" />: 1.0.0
</CustomText> */}

const styles = StyleSheet.create({
  container: {
    paddingTop: 24,
    marginTop: 24,
    flex: 1,
  },
  homeLinkContainer: {
    width: WindowDimensions.width - 2 * 24,
    flexDirection: 'column',
  },
  regButtonStyle: {
    borderWidth: 1,
    borderColor: '#E42320',
    minHeight: 55,
    marginTop: 24,
  },
  buttonStyle: {
    borderWidth: 1,
    borderColor: '#E42320',
    minHeight: 55,
  },
  buttonTextStyle: {
    fontSize: 22,
  },
});
