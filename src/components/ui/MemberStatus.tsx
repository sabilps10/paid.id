import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {WindowDimensions} from '../../utils/WindowDimensions';
import CustomText from './CustomText';

enum MemberType {
  GOLD,
  PLATINUM,
  DIAMOND,
}

let type: MemberType = MemberType.GOLD;

export const MemberStatus: React.FC = () => {
  return (
    <View style={styles.container}>
      <CustomText textType="regular" style={{fontSize: 24, fontWeight: '900'}}>
        Your subscription
      </CustomText>
      {type == MemberType.GOLD ? (
        <View style={[styles.box]}>
          <Text style={styles.text}>GOLD</Text>
          <Text style={styles.bottomText}>next level is diamond</Text>
        </View>
      ) : null}
      {type == MemberType.PLATINUM ? (
        <View style={[styles.box]}>
          <Text style={styles.text}>PLATINUM</Text>
        </View>
      ) : null}
      {type == MemberType.DIAMOND ? (
        <View style={[styles.box]}>
          <Text style={styles.text}>DIAMOND</Text>
        </View>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 24,
    paddingTop: 12,
    flex: 1,
    flexDirection: 'column',
  },
  box: {
    marginTop: 12,
    paddingRight: 5,
    width: WindowDimensions.width - 48,
    height: WindowDimensions.height / 8,
    backgroundColor: '#FCCB00',
    borderRightWidth: 2,
    borderRightColor: '#fff',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  topText: {
    fontSize: 16,
  },
  text: {
    fontSize: 40,
    fontWeight: 'bold',
    color: '#FFF3B9',
    textAlign: 'center',
    textTransform: 'uppercase',
  },
  bottomText: {
    fontSize: 15,
    fontWeight: '800',
    color: '#FFF3B9',
    textAlign: 'center',
    textTransform: 'uppercase',
  },
});
