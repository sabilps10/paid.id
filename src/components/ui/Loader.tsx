import React from 'react';
import {
  Modal,
  View,
  Text,
  ActivityIndicator,
  StyleSheet,
  StatusBar,
} from 'react-native';

export const Loader: React.FC = () => {
  return (
    <Modal>
      <StatusBar barStyle="light-content" />
      <View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size={'large'} color="#fff" />
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ef262d',
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
});
