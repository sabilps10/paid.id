import { View, StyleSheet, Text, KeyboardAvoidingView } from "react-native";
import React, { FunctionComponent } from "react";
import { TextField } from "react-native-material-textfield";
import { WindowDimensions } from "../../utils/WindowDimensions";
import { useTranslate } from "../../hooks/useTranslate";
import { useStores } from "../../stores";
import { CustomInput } from "./CustomInput";

export interface Product {
  name: string;
  quantity: number;
  price: number;
};
type ProductListProps = {
  products: Product[];
  handleChangeProduct(
    key: number,
    text: string,
    field: string,
    toFloat: boolean,
  ): any;
  errorFocus(
    inputRef,
  ): any;
};

const ProductList: FunctionComponent<ProductListProps> = ({
  products,
  handleChangeProduct,
  errorFocus,
}) => {
  const [translate] = useTranslate();
  const errorIndex = products.findIndex(elem => elem.name.length < 5);
  const prodList = products.map((product, key) => {
    return (
      <KeyboardAvoidingView key={key} style={styles.box}>
        <Text style={styles.boxTitle}>{translate('product')}</Text>
        <CustomInput
          ref={(input) => { 
            if (errorIndex == key) { 
              errorFocus(input);
            }
          }}
          label={translate('productName')}
          containerStyle={{ width: WindowDimensions.width80 }}
          keyboardType="default"
          value={product.name}
          onInputChange={text => 
            handleChangeProduct(key, text, 'name', false).catch()
          }
          error={product.name.length < 5? translate('productNameError'): undefined}
        />
        <View style={styles.detailsContaienr}>
          <CustomInput
            label={translate('quantity')}
            containerStyle={{ width: WindowDimensions.width40 }}
            keyboardType="numeric"
            value={product.quantity == 0 ? '':product.quantity.toString()}
            onInputChange={text =>
              handleChangeProduct(key, text, 'quantity', true).catch()

            }
          />
          <CustomInput
            label={translate('price')}
            containerStyle={{ width: WindowDimensions.width40 }}
            keyboardType="numeric"
            value={product.price == 0 ? '':product.price.toString()}
            onInputChange={text => 
              handleChangeProduct(key, text, 'price', true).catch()
            }
          />
        </View>
      </KeyboardAvoidingView>
    );
  });
  return prodList;
}

const styles = StyleSheet.create({
  upContainer: {
    flex: 1,
    backgroundColor: '#fff',
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.08)',
  },
  box: {
    backgroundColor: '#fff',
    padding: 12,
    marginBottom: 12,
  },
  boxTitle: {
    fontSize: 16,
    fontWeight: '800',
    paddingBottom: 4,
  },
  boxDescription: {
    fontSize: 14,
  },
  scrollContainer: {
    paddingTop: 12,
    paddingLeft: 12,
    paddingRight: 12,
    paddingBottom: 33,
  },
  detailsContaienr: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  regButtonStyle: {
    borderWidth: 1,
    borderColor: '#000',
    minHeight: 42,
    marginBottom: 12,
  },
  buttonStyle: {
    borderWidth: 1,
    borderColor: '#000',
    minHeight: 42,
  },
  buttonTextStyle: {
    fontSize: 18,
  },
  amountSize: {
    fontSize: 22,
  },
  bottomContainer: {
    bottom: 0,
    left: 0,
    right: 0,
    padding: 12,
    backgroundColor: '#fff',
    borderTopColor: '#515151',
    borderTopWidth: 1,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});


export default ProductList;