import React from 'react';
import {View, Image, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import CustomText from './CustomText';
import {useContacts} from '../../hooks/useContacts';
import {useStores} from '../../stores';
import {useObserver} from 'mobx-react';
import {Contact} from 'src/stores/ContactStore';
import {useNavigation} from '@react-navigation/native';
import LocalizedText from './LocalizedText';
import NoImage from './NoImage';
import { OpenModal } from '../../stores/ContextStore';

export const RoundContactList: React.FC = () => {
  const [getContacts] = useContacts();
  const {contactStore, contextStore} = useStores();
  const {navigate} = useNavigation();

  const startInvoice = (contact: Contact) => {
    contextStore.openModal(OpenModal.NEWINVOICE, contact);
  };

  return useObserver(() => (
    <View style={{padding: 24, paddingBottom: 0}}>
      <CustomText textType="regular" style={{fontSize: 24, fontWeight: '900'}}>
        My Clients
      </CustomText>
      {contactStore.contacts.length === 0 && (
        <View style={sytles.noContactContainer}>
          <TouchableOpacity
            onPress={() => {
              getContacts();
            }}>
            <CustomText
              style={sytles.noContactContainerText}
              textType={'light'}>
              <LocalizedText content="contacts" />
            </CustomText>
          </TouchableOpacity>
        </View>
      )}
      {contactStore.contacts.length > 0 && (
        <View style={sytles.container}>
          <FlatList
            data={contactStore.contacts}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            keyExtractor={() => Math.random().toString()}
            renderItem={({item}) => {
              let image;
              if (item.image && item.image !== '') {
                image = (
                  <Image source={{uri: item.image}} style={sytles.image} />
                );
              } else {
                image = (
                  <NoImage initials={item.firstName + ' ' + item.lastName} />
                );
              }
              return (
                <TouchableOpacity
                  onPress={() => startInvoice(item)}
                  style={sytles.itemContainer}>
                  {image}
                  <Text>{item.firstName}</Text>
                </TouchableOpacity>
              );
            }}
          />
        </View>
      )}
    </View>
  ));
};

const sytles = StyleSheet.create({
  container: {
    paddingTop: 12,
    paddingBottom: 12,
    paddingRight: 12,
    paddingLeft: 12,
    marginTop: 12,

    backgroundColor: '#fff',
    borderRadius: 10,
  },
  itemContainer: {
    marginRight: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
  },
  noContactContainer: {
    paddingTop: 24,
    paddingBottom: 24,
    marginTop: 12,
    backgroundColor: '#fff',
    borderRadius: 10,
  },
  noContactContainerText: {
    textAlign: 'center',
    fontSize: 18,
  },
});
