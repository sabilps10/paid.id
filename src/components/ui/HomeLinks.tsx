import React from 'react';
import {View, Text, StyleSheet, Button, TouchableOpacity} from 'react-native';
import {WindowDimensions} from '../../utils/WindowDimensions';
import { useObserver } from 'mobx-react';
import LocalizedText from './LocalizedText';
import { useStores } from '../../stores';
import { OpenModal } from '../../stores/ContextStore';

export const HomeLinks: React.FC = () => {
  const { contextStore } = useStores();
  return useObserver(()=>(
    <View style={styles.container}>
      <View style={[styles.homeLinkContainer]}>
      <TouchableOpacity onPress={() => contextStore.openModal(OpenModal.NEWINVOICE)}>
          <View style={styles.invoiceButton}>
            <Text style={styles.invoiceButtonText}>
              <LocalizedText content="createInvoice" />
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  ));
};

const styles = StyleSheet.create({
  container: {
    padding: 24,
    flex: 1,
    flexDirection: 'row',
  },
  homeLinkContainer: {
    paddingRight: 5,
    width: WindowDimensions.width - 2 * 24,
  },
  invoiceButton: {
    backgroundColor: '#FF3A3A',
    paddingRight: 5,
    width: WindowDimensions.width - 2 * 24,
    borderRadius: 10,
    height: WindowDimensions.height / 15,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  invoiceButtonText: {
    textAlign: 'center',
    fontSize: 25,
    color: '#ffffff'
  }
});
