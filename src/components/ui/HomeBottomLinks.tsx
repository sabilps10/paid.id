import React, { useState } from 'react';
import { View, Image, StyleSheet, Text, TouchableOpacity, Share } from 'react-native';
import { useStores } from '../../stores';
import { useTranslate } from '../../hooks/useTranslate';

import linkImage from '../../assets/images/link.png'
import { WindowDimensions } from '../../utils/WindowDimensions';
import CustomText from './CustomText';
import { OpenModal } from '../../stores/ContextStore';

export const HomeBottomLinks = () => { 
  const { contextStore } = useStores();
  const [translate] = useTranslate();
  const [modalState, setModalState] = useState<boolean>(false);
  return (
    <View style={styles.outerContainer}>
      <CustomText textType="regular" style={{fontSize:24, fontWeight:'900'}}>Some links</CustomText>
      <View style={styles.innerContainer}>
        <View style={styles.columnContainer}>
          <TouchableOpacity onPress={()=> contextStore.openModal(OpenModal.WRITEUS)}>
            <View style={[ styles.linkContainer, styles.upperLinkContainer]}>
              <View style={styles.imageContainer}>
                <Image style={styles.image} source={linkImage}/>
              </View>
              <Text style={styles.text}>Write Us</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={[styles.columnContainer, styles.secondColumnContainer]} >
          <TouchableOpacity onPress={() => Share.share({
            message: translate('secondSlideTitle') + `!\n https://paidapp.id/${contextStore.languageCode === 'en'? 'en':''}`
          })}>
            <View style={[ styles.linkContainer, styles.upperLinkContainer]}>
              <View style={styles.imageContainer}>
                <Image style={styles.image} source={linkImage}/>
              </View>
              <Text style={styles.text}>Share</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  imageContainer: {
    width: WindowDimensions.width / 7,
    height: WindowDimensions.width / 7,
    borderRadius: 100,
    alignContent: 'center',
    textAlignVertical: 'center',
    textAlign: 'center',
    alignItems: 'center',
    backgroundColor: '#FCCB00',
    paddingTop: WindowDimensions.width / 32
  },
  innerContainer: { flexDirection: 'row', marginTop: 12, padding: 12, borderRadius: 10, backgroundColor: "#fff" },
  outerContainer: { flexDirection: 'column', padding: 24 },
  columnContainer: { flexDirection: 'column' },
  secondColumnContainer: { marginLeft: WindowDimensions.width / 7 },
  upperLinkContainer: { marginBottom:12 },
  image: {
    width: WindowDimensions.width / 12,
    height: WindowDimensions.width / 12,
  },
  text: {
    marginTop: 16, marginLeft: 16, fontSize: 20
  },
  linkContainer: {flexDirection: 'row', alignContent: 'center' }
});