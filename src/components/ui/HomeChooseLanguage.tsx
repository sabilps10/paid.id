import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {WindowDimensions} from '../../utils/WindowDimensions';
import {Button} from 'react-native-material-ui';
import { useStores } from '../../stores';
import CustomText from './CustomText';
import { useObserver } from 'mobx-react';
import LocalizedText from './LocalizedText';

export const HomeChooseLanguage: React.FC = () => {
  const { contextStore } = useStores();

  return useObserver(() => (
    <View style={styles.outerContainer}>
      <CustomText
        style={styles.noContactContainerText}
        textType={'light'}>
        <LocalizedText content="language" />:  <LocalizedText content="actualLanguage" />
      </CustomText>
      <View style={styles.innerContainer}>
        <View style={[styles.homeLinkContainer]}>
          <Button
            onPress={() => contextStore.setLanguage('id')}
            raised
            primary
            text="Indonesian"
          />
        </View>
        <View style={[styles.homeLinkContainer]}>
          <Button
            onPress={() => contextStore.setLanguage('en')}
            raised
            primary
            text="English"
          />
        </View>
      </View>
    </View>
  ));
};

const styles = StyleSheet.create({
  outerContainer: {
    paddingBottom: 24,
    flex: 1,
    flexDirection: 'column',
  },
  innerContainer: {
    paddingTop: 24,
    flex: 1,
    flexDirection: 'row',
  },
  homeLinkContainer: {
    paddingRight: 5,
    width: WindowDimensions.width / 2 - 24,
  },
  noContactContainer: {
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    paddingTop: 24,
    paddingBottom: 24,
  },
});
