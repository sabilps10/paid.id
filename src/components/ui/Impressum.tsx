import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {WindowDimensions} from '../../utils/WindowDimensions';
import {useNavigation} from '@react-navigation/native';
import {useObserver} from 'mobx-react';
import LocalizedText from './LocalizedText';
import CustomText from './CustomText';

export const Impressum: React.FC = () => {
  const {navigate} = useNavigation();
  return useObserver(() => (
    <View style={styles.container}>
      <View style={[styles.homeLinkContainer]}>
        <TouchableOpacity onPress={() => {}}>
          <View>
            <CustomText
              textType="regular"
              style={{fontSize: 24, fontWeight: '900', textAlign: 'center'}}>
              <LocalizedText content="version" />: 1.0.0
            </CustomText>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            navigate('PrivacyPolicyScreen', {screen: 'PrivacyPolicyScreen'})
          }>
          <View>
            <CustomText
              textType="regular"
              style={{fontSize: 24, fontWeight: '900', textAlign: 'center'}}>
              <LocalizedText content="privacyPolicy" />
            </CustomText>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  ));
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 24,
    marginTop: 24,
    flex: 1,
  },
  homeLinkContainer: {
    width: WindowDimensions.width - 2 * 24,
    flexDirection: 'column',
  },
});
