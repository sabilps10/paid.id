import React from 'react';
import {View, Image, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {FlatList, TouchableHighlight} from 'react-native-gesture-handler';
import CustomText from './CustomText';
import {useContacts} from '../../hooks/useContacts';
import nophoto from '../../assets/images/nophoto.png';
import {useStores} from '../../stores';
import {useObserver} from 'mobx-react';
import {Contact} from 'src/stores/ContactStore';
import {useNavigation} from '@react-navigation/native';
import LocalizedText from './LocalizedText';
import InvoiceStore from 'src/stores/InvoiceStore';
import NoImage from './NoImage';
import { WindowDimensions } from '../../utils/WindowDimensions';
import { OpenModal } from '../../stores/ContextStore';
import { formatAmount } from '../../utils/Amount';

export const RecentsList: React.FC = () => {
  const [getContacts] = useContacts();
  const {contactStore, invoiceStore, contextStore} = useStores();
  const {navigate} = useNavigation();

  const startInvoice = (contact: Contact) => {
    navigate('Invoices', {screen: 'NewInvoice', params: {contact}});
  };

  return useObserver(() => (
    <View>
    {invoiceStore.invoice.length > 0 ?
        <View style={styles.container}>
          <CustomText textType="regular" style={{fontSize:24, fontWeight:'900'}}>Recents</CustomText>
          <View style={styles.flatListOuterContainer}>
          <FlatList
            contentContainerStyle={styles.flatListContainer}
            data={invoiceStore.invoice.slice(invoiceStore.invoice.length - 2).reverse()}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            keyExtractor={() => Math.random().toString()}
            renderItem={({item}) => {
              let image = nophoto;
              /* if (item.image && item.image !== '') {
                image = {uri: 'file:///' + item.image};
              } */
              return (
                <TouchableOpacity
                  onPress={() => { contextStore.openModal(OpenModal.PDFVIEW, item.localInvoiceId) }}
                  style={styles.itemContainer}>
                  {false ?
                    <Image source={image} style={styles.image} /> :
                    <View style={styles.image}>
                      <NoImage initials={item.clientName} />
                    </View>
                    }
                  
                  <View style={styles.itemNameContainer}>
                    <Text style={styles.itemName}>{item.clientName}</Text>
                  </View>
                <Text style={styles.price}>{formatAmount(caculateTotal(item))}</Text>
                </TouchableOpacity>
              );
            }}
            />
            </View>
        </View>: null}
    </View>
  ));
};

const caculateTotal = (item: any) => { 
  let total = item.products.reduce((prev: any, current: any) => { 
    return prev + current.price * current.quantity;
  }, 0);
  total -= total * item.discountPercent / 100;
  total += total * item.taxPercent / 100;
  total += item.shipping;
  return Math.round(total);
}

const styles = StyleSheet.create({
  container: {
    marginLeft: 24,
    marginRight: 24,
    marginTop: 12,
    flex: 1,
    flexDirection: 'column'
  },
  itemContainer: {
    marginRight: 24,
    marginTop: 12,
    width:WindowDimensions.width - 48,
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row'
  },
  flatListContainer: { flexDirection: 'column' },
  flatListOuterContainer: {
    marginTop:12,
    paddingBottom: 12,
    paddingRight: 12,
    backgroundColor: "#fff",
    borderRadius: 10, 

    paddingLeft: 12,
    flex: 1
  },
  image: {
   marginRight: 24,
  },
  itemName: {
    fontSize: 18
  },
  itemNameContainer: {
    width: WindowDimensions.width / 8 * 3
  },
  price: {
    fontSize: 18,
    marginRight: 12
  }
});
