import React, { FunctionComponent } from 'react';
import { Text, TextStyle } from 'react-native';
import { useObserver } from 'mobx-react';
import { useStores } from '../../stores';
import { useTranslate } from '../../hooks/useTranslate';

interface LocalizedTextProps { 
  content: string
  style?: TextStyle | TextStyle[];
};

const LocalizedText: FunctionComponent<LocalizedTextProps> = ({ content, style }) => {
  const [translate] = useTranslate();
  const passedStyles = Array.isArray(style)
  ? Object.assign({}, ...style)
  : {...style};
  return  useObserver(() => (<Text style={passedStyles}>{translate(content)}</Text>));
};

export default LocalizedText;