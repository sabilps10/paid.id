import React, { useState, forwardRef, RefObject, Ref } from 'react';
import {
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ViewStyle,
  TextStyle,
  TextInputProps,
} from 'react-native';
import CustomText from './CustomText';

export type InputOnTextChange = (text: string) => void;

export interface ICustomInput {
  containerStyle?: ViewStyle;
  inputStyle?: TextStyle;
  placeholder?: string;
  placeHolderColor?: string;
  buttonText?: string;
  label?: string;
  error?: string;
  errorColor?: string;
  errorInputColor?: string;
  isMultiline?: boolean;
  autoCapitalize?: "none" | "sentences" | "words" | "characters" | undefined;
  keyboardType?: "default" | "email-address" | "numeric" | "phone-pad" | "visible-password" | "ascii-capable" | "numbers-and-punctuation" | "url" | "number-pad" | "name-phone-pad" | "decimal-pad" | "twitter" | "web-search" | undefined;
  onInputChange?: InputOnTextChange;
  value: string;
  inputProps?: TextInputProps;
  ref?(input: TextInput);
  disabled?: boolean;
  secureTextEntry?: boolean;
}

export interface ICustomInputLayout {
  fontStyle: TextStyle;
  style: ViewStyle;
  inputPaddingRight: number;
}

export const CustomInput = forwardRef<TextInput, ICustomInput>(({
  containerStyle,
  inputStyle,
  placeholder,
  placeHolderColor,
  buttonText,
  label,
  error,
  errorColor,
  errorInputColor,
  isMultiline,
  autoCapitalize,
  keyboardType,
  onInputChange,
  value,
  inputProps,
  disabled,
  secureTextEntry
}, ref) => {
  const [layout, setLayout] = useState<ICustomInputLayout>({
    fontStyle: {},
    style: {},
    inputPaddingRight: 10,
  });

  return (
    <View style={[styles.container, { ...containerStyle }]}>
      {label && <CustomText textType="regular" style={{fontSize:16, fontWeight:'900', marginBottom:8, color: errorColor || '#000000'}}>
        {label}
      </CustomText>}
      <TextInput
        multiline={isMultiline || false}
        autoCapitalize={autoCapitalize || 'sentences'}
        placeholderTextColor={placeHolderColor || '#8e8e8e'}
        placeholder={placeholder || ''}
        onChangeText={onInputChange}
        value={value}
        style={[styles.input, { paddingRight: layout.inputPaddingRight }, inputStyle || {}, error ? { backgroundColor: errorInputColor || '#E6D4D4' } : {}]}
        keyboardType={keyboardType || "default"}
        onLayout={({ nativeEvent }) => {
          setLayout({
            ...layout,
            style: {
              ...layout.style,
              height: nativeEvent.layout.height,
            },
          });
        }}
        {...inputProps}
        ref={ref}
        editable={!disabled}
        secureTextEntry={secureTextEntry}
      />
      {error && <CustomText textType="regular" style={[{ fontSize: 14, fontWeight: '900', color: '#E42320', marginTop: 8, marginLeft: 8 }, errorColor ? { color: errorColor } : {}]}>
        {error}
      </CustomText>}
      {buttonText && (
        <TouchableOpacity
          onLayout={({nativeEvent}) => {
            setLayout({
              ...layout,
              inputPaddingRight: nativeEvent.layout.width + 2,
            });
          }}
          style={[styles.rightTouchableButton, layout.style]}>
          <CustomText
            textType={'bold'}
            style={[styles.fontStyle, layout.fontStyle]}>
            {buttonText}
          </CustomText>
        </TouchableOpacity>
      )}
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
  },
  input: {
    position: 'relative',
    fontFamily: 'OpenSans-Light',
    backgroundColor: '#e6e6e6',
    color: '#000',
    fontSize: 18,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 14,
    paddingBottom: 14,
    borderRadius: 10,
  },
  rightTouchableButton: {
    position: 'absolute',
    right: 0,
    height: 10,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#000',
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  fontStyle: {
    color: '#fff',
    fontSize: 18,
  },
});
