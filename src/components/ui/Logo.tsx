import React from 'react';
import CustomText from './CustomText';
import {View, StyleSheet} from 'react-native';
import {ViewStyle, TextStyle} from 'react-native-material-ui';

export interface LogoProps {
  containerStyle?: ViewStyle;
  textStyle?: TextStyle;
}

export const Logo: React.FC<LogoProps> = ({containerStyle, textStyle}) => {
  return (
    <View style={[containerStyle]}>
      <CustomText textType={'anton'} style={[styles.logoText, {...textStyle}]}>
        PA.ID
      </CustomText>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
  logoText: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 44,
  },
});
