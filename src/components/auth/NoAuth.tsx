import React from 'react';
import {StyleSheet, View} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import {WindowDimensions} from '../../utils/WindowDimensions';
import {Button} from 'react-native-material-ui';
import CustomText from '../ui/CustomText';
import {Logo} from '../ui/Logo';
import { useTranslate } from '../../hooks/useTranslate';
import { useStores } from '../../stores';

type onPress = () => void;

interface NoAuthProps {
  nextToAuth: onPress;
}

export const NoAuth: React.FC<NoAuthProps> = ({ nextToAuth }) => {
  const { contextStore } = useStores();
  const [translate] = useTranslate();
  const renderItem = ({item}) => {
    return (
      <View style={styles.slide}>
        <CustomText textType={'anton'} style={styles.title}>
          {item.title}
        </CustomText>
        <CustomText textType={'regular'} style={styles.text}>
          {item.text}
        </CustomText>
      </View>
    );
  };

  const overButton = (
    <View style={styles.buttonContainer}>
      <Button
        upperCase={false}
        style={{
          container: styles.buttonStyle,
          text: styles.buttonTextStyle,
        }}
        onPress={nextToAuth}
        accent
        text={translate('createAnAccount')}
      />
    </View>
  );

  return (
    <View style={styles.container}>
      <Logo containerStyle={{marginTop: 24}} />
      <AppIntroSlider
        showPrevButton={false}
        showNextButton={false}
        showSkipButton={true}
        renderItem={renderItem}
        bottomButton={true}
        data={slides}
        renderDoneButton={() => overButton}
        renderSkipButton={() => overButton}
        onDone={() => {}}
      />
    </View>
  );
};

const slides = [
  {
    key: '1',
    title: 'GET PAID UPFRONT',
    text: 'Kirim invoice profesional Anda kapan saja, di mana saja, ke mana saja.',
  },
  {
    key: '2',
    title: 'MOVE YOUR BUSINESS TO WHATSAPP',
    text: 'Cara paling sederhana untuk urusan keuangan anda.',
  },
  {
    key: '3',
    title: 'COLLECT IN ANY PAYMENT METHOD.',
    text:
      'Dengan kartu kredit, kartu debit, dompet elektronik, transfer bank, dan berbagai metode pembayaran lainnya.',
  },
];
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#E42320',
    minHeight: WindowDimensions.height * 0.9,
  },
  slide: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: WindowDimensions.height / 12,
    
    //paddingBottom: 96, // Add padding to offset large buttons and pagination in bottom of page
  },
  title: {
    fontSize: 45,
    lineHeight: 55,
    color: '#fff',
    textAlign: 'left',
    paddingLeft: 24,
    paddingRight: 24,
    fontWeight: '800',
    width: WindowDimensions.width,
  },
  text: {
    fontSize: 26,
    textAlign: 'left',
    paddingLeft: 24,
    paddingRight: 24,
    width: WindowDimensions.width,
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: 75,
  },
  buttonStyle: {
    borderWidth: 1,
    borderColor: '#fff',
    minHeight: 55,
    width: WindowDimensions.width90,
  },
  buttonTextStyle: {
    fontSize: 22,
  },
});
