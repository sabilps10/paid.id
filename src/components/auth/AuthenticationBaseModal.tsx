import React, {useState} from 'react';
import {Modal, StyleSheet, View, StatusBar} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {NoAuth} from './NoAuth';
import {Auth} from './Auth';

export const AuthenticationModalBase: React.FC = () => {
  const [visibleState, setVisibleState] = useState<boolean>(true);
  const [authState, setAuthState] = useState('no-auth');
  return (
    <Modal visible={visibleState}>
      <StatusBar barStyle={'light-content'} />
      <SafeAreaView style={styles.container}>
        {authState === 'no-auth' && (
          <NoAuth nextToAuth={() => setAuthState('auth')} />
        )}
        {authState === 'auth' && (
          <Auth onClose={() => setVisibleState(false)} />
        )}
      </SafeAreaView>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#E42320',
    flex: 1,
    //alignItems: 'center',
    //justifyContent: 'center',
    //minHeight: 800,
  },
});
