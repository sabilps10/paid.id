import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
  Keyboard,
  Alert,
} from 'react-native';
import {WindowDimensions} from '../../utils/WindowDimensions';
import {Button} from 'react-native-material-ui';
import fb from '../../assets/images/fb.png';
import gmail from '../../assets/images/gmail.png';

import {Title} from '../ui/Title';
import {useFacebookAuth} from '../../hooks/useFacebookAuth';
import {useGoogleAuth} from '../../hooks/useGoogleAuth';
import {Logo} from '../ui/Logo';
import {
  useSignUpMutation,
  AuthWithSocialMutation,
  useLogInMutation,
} from '../../graphql';
import {useStores} from '../../stores';
import {Loader} from '../ui/Loader';
import {useTranslate} from '../../hooks/useTranslate';
import {CustomInput} from '../ui/CustomInput';
import {useMe} from '../../hooks/useMe';

export type onPress = () => void;

export type AuthType = 'NONE' | 'EMAIL' | 'GOOGLE' | 'FACEBOOK';
const NONE: AuthType = 'NONE';
const EMAIL: AuthType = 'EMAIL';
const GOOGLE: AuthType = 'GOOGLE';
const FACEBOOK: AuthType = 'FACEBOOK';

export type AuthMethod = 'LOGIN' | 'REGISTRATION';
const LOGIN: AuthMethod = 'LOGIN';
const REGISTRATION: AuthMethod = 'REGISTRATION';

export type AuthProcess = 'DEFAULT' | 'STARTED' | 'SUCCESS' | 'FAILED';
const DEFAULT: AuthProcess = 'DEFAULT';
const STARTED: AuthProcess = 'STARTED';

export type AuthError = 'OK' | 'COMPANYERROR' | 'EMAILERROR' | 'PASSWORDERROR';
const COMPANYERROR: AuthError = 'COMPANYERROR';
const EMAILERROR: AuthError = 'EMAILERROR';
const PASSWORDERROR: AuthError = 'PASSWORDERROR';

export interface AuthPros {}

export interface AuthData {
  companyName: string;
  email: string;
  password: string;
}

export interface AuthSateProps {
  authType: AuthType;
  authMethod: AuthMethod;
  authProcess: AuthProcess;
  authError: AuthError[];
}

export const Auth: React.FC<AuthPros> = () => {
  const [authData, setAuthData] = useState<AuthData>({
    companyName: '',
    email: '',
    password: '',
  });
  const [keyBoardData, setKeyBoardData] = useState<boolean>(true);

  const completeAuth = (data: AuthWithSocialMutation) => {
    if (data && data.authWithSocial.token) {
      userStore.saveAuth({
        email: data.authWithSocial.user.email,
        companyName: data.authWithSocial.user.companyName || '',
        companyAddress: data.authWithSocial.user.companyAddress || '',
        companyNumber: data.authWithSocial.user.companyNumber || '',
        companyWebsite: data.authWithSocial.user.companyWebsite || '',
        companyEmailAdress: data.authWithSocial.user.companyEmailAdress || '',
        socialId: data.authWithSocial.user.socialId || '',
        firstName: data.authWithSocial.user.firstName || '',
        lastName: data.authWithSocial.user.lastName || '',
        token: data.authWithSocial.token,
        tokenExpire: data.authWithSocial.tokenExpire,
        id: data.authWithSocial.user.id,
      });
    }
  };

  const [authWithFacebook] = useFacebookAuth({
    onCompleted: completeAuth,
    onError: () => {
      Alert.alert('Registartion unsuccessfull! Try it again!');
      setAuthState({...authState, authProcess: DEFAULT});
    },
    onSocialError: () => {
      Alert.alert('Registartion unsuccessfull! Try it again!');
      setAuthState({...authState, authProcess: DEFAULT});
    },
  });
  const {authWithGoogle} = useGoogleAuth({
    onCompleted: completeAuth,
    onError: () => {
      Alert.alert('Registartion unsuccessfull! Try it again!');
      setAuthState({...authState, authProcess: DEFAULT});
    },
    onSocialError: () => {
      Alert.alert('Registartion unsuccessfull! Try it again!');
      setAuthState({...authState, authProcess: DEFAULT});
    },
  });

  const {userStore} = useStores();
  const [translate] = useTranslate();

  const [authMutation] = useSignUpMutation({
    variables: {
      companyName: authData.companyName,
      email: authData.email,
      password: authData.password,
    },
    onCompleted: data => {
      if (data && data.signUp.token) {
        userStore.saveAuth({
          email: data.signUp.user.email,
          companyName: data.signUp.user.companyName || '',
          companyAddress: data.signUp.user.companyAddress || '',
          companyNumber: data.signUp.user.companyNumber || '',
          companyWebsite: data.signUp.user.companyWebsite || '',
          companyEmailAdress: data.signUp.user.companyEmailAdress || '',
          socialId: data.signUp.user.socialId || '',
          firstName: data.signUp.user.firstName || '',
          lastName: data.signUp.user.lastName || '',
          token: data.signUp.token,
          tokenExpire: data.signUp.tokenExpire,
          id: data.signUp.user.id,
        });
      }
    },
    onError: error => {
      console.log(error.graphQLErrors[0].message);
      if (
        error.graphQLErrors[0].message == 'authentication.error.email.notunique'
      ) {
        Alert.alert(
          'Registartion unsuccessfull! This email is already registrated. Try it again!',
        );
      } else {
        Alert.alert('Registartion unsuccessfull! Try it again!');
      }
      setAuthState({
        ...authState,
        authProcess: DEFAULT,
      });
    },
  });
  const [syncMe] = useMe();
  const [logInMutation] = useLogInMutation({
    variables: {
      email: authData.email,
      password: authData.password,
    },
    onCompleted: data => {
      if (data && data.logIn.token) {
        userStore.saveAuth({
          email: data.logIn.user.email,
          companyName: data.logIn.user.companyName || '',
          socialId: data.logIn.user.socialId || '',
          firstName: data.logIn.user.firstName || '',
          lastName: data.logIn.user.lastName || '',
          token: data.logIn.token,
          tokenExpire: data.logIn.tokenExpire,
          id: data.logIn.user.id,
          companyAddress: data.logIn.user.companyAddress || '',
          companyEmailAdress: data.logIn.user.companyEmailAdress || '',
          companyNumber: data.logIn.user.companyNumber || '',
          companyWebsite: data.logIn.user.companyWebsite || '',
        });
        syncMe();
      }
    },
    onError: () => {
      Alert.alert('Login unsuccessfull! Try it again!');
      setAuthState({...authState, authProcess: DEFAULT});
    },
  });

  const [authState, setAuthState] = useState<AuthSateProps>({
    authType: NONE,
    authMethod: REGISTRATION,
    authProcess: DEFAULT,
    authError: [],
  });

  useEffect(() => {
    if (authState.authProcess === STARTED) {
      if (
        authState.authType === EMAIL &&
        authState.authMethod === REGISTRATION
      ) {
        if (authState.authError.length == 0) {
          authMutation();
        } else {
          setAuthState({...authState, authProcess: DEFAULT});
        }
      }

      if (authState.authType === EMAIL && authState.authMethod === LOGIN) {
        if (authState.authError.length == 0) {
          logInMutation();
        } else {
          setAuthState({...authState, authProcess: DEFAULT});
        }
      }

      if (authState.authType === FACEBOOK) {
        authWithFacebook();
      }
      if (authState.authType === GOOGLE) {
        authWithGoogle();
      }
    }
    return () => {
      authState.authProcess = DEFAULT;
    };
  }, [
    authMutation,
    authWithFacebook,
    authState.authProcess,
    authState.authType,
    authState.authMethod,
    authWithGoogle,
    logInMutation,
    authState,
  ]);

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => setKeyBoardData(true),
    );
    const keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => setKeyBoardData(false),
    );
    setKeyBoardData(false);
    return () => {
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
    };
  }, []);

  const validateInput = (): AuthError[] => {
    let errors: AuthError[] = [];
    if (
      authData.companyName.length < 5 &&
      authState.authMethod === REGISTRATION
    ) {
      errors.push(COMPANYERROR);
    }
    if (authData.email.match(/.+@.+\..+/) == null) {
      errors.push(EMAILERROR);
    }
    if (authData.password.length < 6) {
      errors.push(PASSWORDERROR);
    }
    return errors;
  };
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Logo containerStyle={{marginTop: 24}} />
      {authState.authProcess === STARTED ? <Loader /> : null}
      <Title
        title={
          authState.authMethod === REGISTRATION
            ? translate('createAnAccount')
            : translate('login')
        }
        containerStyle={{marginBottom: 12}}
      />
      {authState.authMethod === REGISTRATION && (
        <CustomInput
          inputStyle={{backgroundColor: 'white'}}
          label={translate('companyName')}
          keyboardType="default"
          value={authData.companyName}
          onInputChange={text =>
            setAuthData({
              ...authData,
              companyName: text,
            })
          }
          error={
            authState.authError.find(elem => elem === COMPANYERROR)
              ? translate('companyNameError')
              : undefined
          }
          errorColor="#ffffff"
          errorInputColor="#FAD7D6"
        />
      )}
      <CustomInput
        inputStyle={{backgroundColor: 'white'}}
        label={translate('email')}
        autoCapitalize={'none'}
        keyboardType="email-address"
        value={authData.email}
        onInputChange={text =>
          setAuthData({
            ...authData,
            email: text,
          })
        }
        error={
          authState.authError.find(elem => elem === EMAILERROR)
            ? 'Invalid e-mail'
            : undefined
        }
        errorColor="#ffffff"
        errorInputColor="#FAD7D6"
      />
      <CustomInput
        inputStyle={{backgroundColor: 'white'}}
        autoCapitalize={'none'}
        label={translate('password')}
        secureTextEntry={true}
        value={authData.password}
        onInputChange={text =>
          setAuthData({
            ...authData,
            password: text,
          })
        }
        error={
          authState.authError.find(elem => elem === PASSWORDERROR)
            ? translate('passwordError')
            : undefined
        }
        errorColor="#ffffff"
        errorInputColor="#FAD7D6"
      />
      <Button
        upperCase={false}
        style={{
          container: styles.regButtonStyle,
          text: styles.buttonTextStyle,
        }}
        onPress={() => {
          let errors: AuthError[] = validateInput();
          if (errors.length != 0) {
            setAuthState({
              ...authState,
              authError: errors,
            });
          } else {
            setAuthState({
              ...authState,
              authError: [],
              authProcess: STARTED,
              authType: EMAIL,
            });
          }
        }}
        accent
        text={
          authState.authMethod === REGISTRATION
            ? translate('letsDoIt')
            : translate('login')
        }
      />
      <TouchableOpacity
        onPress={() =>
          setAuthState({
            ...authState,
            authMethod:
              authState.authMethod === REGISTRATION ? LOGIN : REGISTRATION,
          })
        }
        style={{marginTop: 12}}>
        <Text style={styles.hasAccount}>
          {authState.authMethod === REGISTRATION
            ? translate('alreadyHaveAnAccount')
            : translate('backToRegistration')}
        </Text>
      </TouchableOpacity>
      {!keyBoardData && (
        <View style={styles.socialButtonContainer}>
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              onPress={() =>
                setAuthState({
                  ...authState,
                  authProcess: STARTED,
                  authType: FACEBOOK,
                })
              }>
              <Image source={fb} style={styles.socialButton} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                setAuthState({
                  ...authState,
                  authProcess: STARTED,
                  authType: GOOGLE,
                })
              }>
              <Image source={gmail} style={styles.socialButton} />
            </TouchableOpacity>
          </View>
        </View>
      )}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 24,
    justifyContent: 'center',
    minHeight: WindowDimensions.height,
  },
  buttonContainer: {
    bottom: 0,
    display: 'flex',
    flexDirection: 'row',
    marginTop: 24,
    justifyContent: 'space-between',
  },
  regButtonStyle: {
    borderWidth: 1,
    borderColor: '#fff',
    minHeight: 55,
    marginTop: 24,
  },
  buttonStyle: {
    borderWidth: 1,
    borderColor: '#fff',
    minHeight: 55,
  },
  buttonTextStyle: {
    fontSize: 22,
  },
  logo: {
    height: WindowDimensions.width55,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  socialButtonContainer: {
    position: 'absolute',
    width: WindowDimensions.width - 48,
    bottom: 24,
    alignSelf: 'center',
  },
  socialButton: {
    width: 35,
    height: 35,
    resizeMode: 'contain',
  },
  hasAccount: {
    fontSize: 16,
    color: '#fff',
    textAlign: 'center',
  },
});
