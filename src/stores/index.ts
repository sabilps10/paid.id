import {useContext} from 'react';
import {configure} from 'mobx';
import remotedev from 'mobx-remotedev';
import {AsyncTrunk} from 'mobx-sync';
import {MobXProviderContext} from 'mobx-react';
import AsyncStorage from '@react-native-community/async-storage';
import {ContextStore} from './ContextStore';
import {UserStore} from './UserStore';
import {ContactStore} from './ContactStore';
import InvoiceStore from './InvoiceStore';
configure({
  enforceActions: 'always',
});
const contextStore = new ContextStore();
const userStore = new UserStore();
const contactStore = new ContactStore();
const invoiceStore = new InvoiceStore();

const stores = {
  contextStore: remotedev(contextStore, {name: 'contextStore'}),
  userStore: remotedev(userStore, {name: 'userStore', global: true}),
  contactStore: remotedev(contactStore, {name: 'contactStore'}),
  invoiceStore: remotedev(invoiceStore, {name: 'invoiceStore'}),
};
const trunk = new AsyncTrunk(stores, {storage: AsyncStorage});

void trunk.init().then(stores.contextStore.persistedDataHasLoaded);

export function createStores() {
  return stores;
}

export type Stores = typeof stores;
export function useStores(): Stores {
  return useContext(MobXProviderContext);
}
