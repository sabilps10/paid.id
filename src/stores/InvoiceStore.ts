import {observable, action} from 'mobx';
import {InvoiceFragmentFragment} from 'src/graphql';

export interface Product {
  name: string;
  quantity: number;
  price: number;
}

export interface Invoice {
  localInvoiceId: string;
  clientName: string;
  clinetAddress: string;
  clientEmail: string;
  invoiceNumber: string;
  payTerm: Date;
  products: Array<Product>;
  taxPercent: number;
  discountPercent: number;
  shipping: number;
  comment: string;
  invoiceLocalPath?: string;
  invoiceUrlPath?: string;
  subTotal: number;
  tax: number;
  total: number;
}

export class InvoiceStore {
  @observable public invoice: Array<Invoice> = [];

  @action.bound
  saveInvoice(data: Invoice) {
    this.invoice.push(data);
  }
  @action.bound
  deleteInvoice(invoiceNumber: string) {
    const index = this.invoice.findIndex((element) => element.invoiceNumber == invoiceNumber);
    if (index > -1) { 
      this.invoice.splice(index, 1);
    }
  }

  @action.bound
  reloadInvoice(datas: Array<InvoiceFragmentFragment>) {
    this.reset();
    datas.map(inv => {
      this.saveInvoice({
        localInvoiceId: Math.random().toString(),
        clientName: inv.clientName || '',
        clientEmail: inv.clientEmail || '',
        clinetAddress: inv.clinetAddress || '',
        invoiceNumber: inv.invoiceNumber || '',
        payTerm: inv.payTerm || new Date(),
        products: inv.products,
        taxPercent: inv.taxPercent || 0,
        discountPercent: inv.discountPercent || 0,
        shipping: inv.shipping || 0,
        comment: inv.comment || '',
        invoiceLocalPath: inv.id || undefined,
        invoiceUrlPath: inv.pdfUrl || undefined,
        total: inv.total || 0,
        tax: inv.tax || 0,
        subTotal: inv.subTotal || 0,
      });
    });
  }

  @action.bound
  updateInvoiceLocalPath(localInvoiceId: string, invoiceLocalPath: string) {
    let selectedInvoiceIndex = this.invoice.findIndex(
      invoice => invoice.localInvoiceId === localInvoiceId,
    );

    if (selectedInvoiceIndex > -1) {
      this.invoice[selectedInvoiceIndex].invoiceLocalPath = invoiceLocalPath;
    }
  }

  @action.bound
  reset() {
    this.invoice = [];
  }
}
export default InvoiceStore;
