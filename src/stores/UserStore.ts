import {observable, action} from 'mobx';
import AsyncStorage from '@react-native-community/async-storage';

export interface SaveAuthenticationDataProps {
  id: string;
  socialId: string;
  token: string;
  tokenExpire?: Date;
  email: string;
  firstName?: string;
  lastName?: string;
  companyName?: string;
  companyNumber: string;
  companyWebsite: string;
  companyEmailAdress: string;
  companyAddress: string;
}

export interface PublicUserDataProps {
  companyNumber: string;
  companyWebsite: string;
  companyEmailAdress: string;
  companyName: string;
  companyAddress: string;
}

export class UserStore {
  @observable public companyName? = '';
  @observable public firstName? = '';
  @observable public lastName? = '';
  @observable public email = '';
  @observable public token = '';
  @observable public tokenExpire?: Date;
  @observable public socialId: string | null = null;
  @observable public id?: string;
  @observable public isLoggedIn: boolean = false;

  //additional datas
  @observable public companyNumber: string = '';
  @observable public companyWebsite: string = '';
  @observable public companyEmailAdress: string = '';
  @observable public companyAddress: string = '';

  //some sync info
  @observable public lastSync?: Date = undefined;

  @action.bound
  saveAuth(data: SaveAuthenticationDataProps) {
    //save user
    this.companyName = data.companyName;
    this.lastName = data.lastName;
    this.firstName = data.lastName;
    this.email = data.email;
    this.tokenExpire = data.tokenExpire;
    this.token = data.token;
    this.socialId = data.socialId;
    this.id = data.id;
    this.isLoggedIn = true;
    this.companyAddress = data.companyAddress;
    this.companyNumber = data.companyNumber;
    this.companyWebsite = data.companyWebsite;
    this.companyEmailAdress = data.companyEmailAdress;

    AsyncStorage.setItem('token', this.token);
  }

  @action.bound
  savePublicData(data: PublicUserDataProps) {
    this.companyName = data.companyName;
    this.companyAddress = data.companyAddress;
    this.companyNumber = data.companyNumber;
    this.companyWebsite = data.companyWebsite;
    this.companyEmailAdress = data.companyEmailAdress;
  }

  @action.bound
  reset() {
    this.id = undefined;
    this.email = '';
    this.companyName = '';
    this.firstName = '';
    this.lastName = '';
    this.socialId = null;
    this.isLoggedIn = false;
    this.companyAddress = '';
    this.companyNumber = '';
    this.companyWebsite = '';
    this.companyEmailAdress = '';
    this.lastSync = undefined;
  }

  @action.bound
  public async logout() {
    try {
      //logout
    } catch (error) {
      console.log(error);
    }
    this.reset();
  }

  @action.bound
  public setLastSyncDate(date: Date) {
    this.lastSync = date;
    return true;
  }
}
export default UserStore;
