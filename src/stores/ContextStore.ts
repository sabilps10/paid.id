import {observable, action} from 'mobx';
import { ignore } from 'mobx-sync';

export enum OpenModal { 
  NONE,
  WRITEUS,
  NEWINVOICE,
  PDFVIEW
}

export class ContextStore {
  @ignore
  @observable
  persistedData = true;

  @observable 
  languageCode = 'id'; 
  
  @ignore
  @observable 
  modal: OpenModal = OpenModal.NONE; 

  @ignore
  @observable 
  modalData = undefined; 

  @action.bound
  persistedDataHasLoaded() {
    this.persistedData = false;
  }

  @action.bound
  setLanguage(data: string) {
    this.languageCode = data;
  }

  @action.bound
  openModal(modal: OpenModal, data) {
    this.modalData = data || undefined;
    this.modal = modal;
  }

  @action.bound
  closeModals() {
    this.modal = OpenModal.NONE;
  }
}
export default ContextStore;
