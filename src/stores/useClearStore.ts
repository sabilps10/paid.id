import React from 'react';
import {useStores} from '.';

export const useClearStore = () => {
  const {userStore, contactStore, invoiceStore} = useStores();

  const clearAll = () => {
    userStore.reset();
    contactStore.reset();
    invoiceStore.reset();
  };

  return [clearAll];
};
