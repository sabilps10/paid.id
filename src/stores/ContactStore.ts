import {observable, action} from 'mobx';

export interface Contact {
  firstName: string;
  lastName: string;
  companyName: string;
  companyAddress: string;
  phoneNumber: string;
  email: string;
  image: string;
}

export class ContactStore {
  @observable public contacts: Array<Contact> = [];

  @action.bound
  saveContacts(data: Array<Contact>) {
    //save user
    this.contacts = data;
  }

  @action.bound
  reset() {
    this.contacts = [];
  }
}
export default ContactStore;
