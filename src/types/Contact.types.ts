export interface Contact {
  name: string;
  image?: string;
}
