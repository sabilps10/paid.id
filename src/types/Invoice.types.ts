import {Moment} from 'moment';

export interface Invoice {
  id: string;
  invoiceNumber: string;
  clientName: string;
  termDate: Moment;
  amount: number;
  isPaid: boolean;
}
