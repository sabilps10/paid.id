import React, {useState, useEffect} from 'react';
import Contacts from 'react-native-contacts';
import {useStores} from '../stores';
import first from 'lodash/first';
import { Platform, PermissionsAndroid, Alert } from 'react-native';
import {PERMISSIONS, RESULTS, check, request} from 'react-native-permissions';


export interface Contact {
  firstName: string;
  lastName: string;
  companyName: string;
  companyAddress: string;
  phoneNumber: string;
  email: string;
  image: string;
}

export const useContacts = () => {
  const [contacts, setContacts] = useState<Array<Contact>>([]);
  const {contactStore} = useStores();

  const getContacts = () => {

    if(Platform.OS === 'ios') {
      Contacts.getAll((err, conts) => {
        if (err) {
          throw err;
        }
        setContacts([
          ...conts.map(cont => {
            const address = first(cont.postalAddresses);
            const city = address?.city || '';
            const country = address?.country || '';
            const street = address?.street || '';
            const postCode = address?.postCode || '';
            
            return {
              firstName: cont.givenName,
              lastName: cont.familyName,
              image: cont.thumbnailPath,
              companyName: cont.company || '',
              companyAddress: [street, city, postCode, country].join(' '),
              email: first(cont.emailAddresses)?.email || '',
              phoneNumber: first(cont.phoneNumbers)?.number || '',
            }
          }),
        ]);
      });
    }
    
    if(Platform.OS === 'android') {
      console.log('permisssionre');
      check(PERMISSIONS.ANDROID.READ_CONTACTS)
    .then(result => {
      console.log('resut', result);
        switch (result) {
        case RESULTS.UNAVAILABLE:
          requestPermission();
            break;
        case RESULTS.DENIED:
          requestPermission();
          break;
        case RESULTS.GRANTED:
          console.log('h3')
          Contacts.getAll((err, conts) => {
            if (err) {
              throw err;
            }
            if(conts.length === 0) {
              Alert.alert('You don\'t have any contacts');
            }
            setContacts([
              ...conts.map(cont => {
                const address = first(cont.postalAddresses);
                const city = address?.city || '';
                const country = address?.country || '';
                const street = address?.street || '';
                const postCode = address?.postCode || '';
                
                return {
                  firstName: cont.givenName,
                  lastName: cont.familyName,
                  image: cont.thumbnailPath,
                  companyName: cont.company || '',
                  companyAddress: [street, city, postCode, country].join(' '),
                  email: first(cont.emailAddresses)?.email || '',
                  phoneNumber: first(cont.phoneNumbers)?.number || '',
                }
              }),
            ]);
          });
          break;
          case RESULTS.BLOCKED:
          console.log('h4')
            break;
        }
    })
    .catch(error => {
      console.log(error)
    });
    }


  };

  const requestPermission = () => {
    request(PERMISSIONS.ANDROID.READ_CONTACTS).then(result => {
      if(result === 'granted') {
        getContacts();
      }else {
      }
    });
  }

  useEffect(() => {
    if (contacts.length > 0) {
      contactStore.saveContacts(contacts);
    }
  }, [contactStore, contacts, contacts.length]);

  return [getContacts];
};
