import React, {useState, useEffect} from 'react';
import {useMeLazyQuery} from '../graphql';
import {useStores} from '../stores';
import moment from 'moment';

export const useMe = () => {
  const {invoiceStore, userStore} = useStores();
  const [syncMe, {loading}] = useMeLazyQuery({
    onCompleted: data => {
      console.log('syncmee55, completed', data);
      if(data.me.user)
        userStore.savePublicData({
          companyNumber: data.me.user.companyNumber,
          companyWebsite: data.me.user.companyWebsite,
          companyEmailAdress: data.me.user.companyEmailAdress,
          companyName: data.me.user.companyName,
          companyAddress: data.me.user.companyAddress
        });
      if (data.me.invoices) {
        invoiceStore.reloadInvoice(data.me.invoices);
      }
    },
  });

  return [syncMe];
};
