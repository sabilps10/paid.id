import React, { useState, useEffect } from 'react';
import {LoginManager, AccessToken} from 'react-native-fbsdk';
import {useAuthWithSocialMutation, Social_Type, AuthWithSocialMutation} from '../graphql';
import {Alert} from 'react-native';
import { ApolloError } from 'apollo-boost';

export type CompletedFunction = (data: AuthWithSocialMutation) => void;
export type ErrorFunction = (data: ApolloError) => void;
export type SocialErrorFunction = (error: any) => void;


export interface useFacebookAuthProps {
  onCompleted: CompletedFunction;
  onError: ErrorFunction;
  onSocialError: SocialErrorFunction;
}

export const useFacebookAuth = ({onCompleted, onError, onSocialError}: useFacebookAuthProps) => {
  
  const [accessToken, setAccessToken] = useState<string>('');

  const updateAccessToken = (token: string) => {
    setAccessToken(token)
  }

  const [mutation] = useAuthWithSocialMutation({
    variables: {
      token: accessToken,
      socialType: Social_Type.Facebook
    },
    onCompleted: data => onCompleted(data),
    onError: error => onError(error),
  });

  useEffect(() => {
   if(accessToken !== '') {
      mutation()
   }
  }, [accessToken])

  
  const authWithFacebook = () => LoginManager.logInWithPermissions(['public_profile, email']).then(
    function(result) {
      console.log(result)
      if (result.isCancelled) {
        onSocialError({isCancelled: true});
      } else {
        AccessToken.getCurrentAccessToken().then(data => {
          const token = data?.accessToken.toString();
          if (token) {
            setAccessToken(data?.accessToken || '');
          }else {
            onSocialError({error: true});
          }
        });
      }
    },
    function(error) {
      console.log('Login fail with error: ' + error);
      onSocialError({error: true});
    },
  );

  return [authWithFacebook];
};
