import React, {useState, useEffect} from 'react';
import { useStores } from '../stores';
import { translate } from '../utils/Translate'

export const useTranslate = () => {
  const { contextStore } = useStores();
  const translateHook = (wordId: string): string => {
    return translate(wordId, contextStore.languageCode);
  };

  return [translateHook];
};
