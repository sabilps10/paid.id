import React, {useState, useEffect} from 'react';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';
import {
  useAuthWithSocialMutation,
  Social_Type,
  AuthWithSocialMutation,
} from '../graphql';
import {ApolloError} from 'apollo-boost';
import {Platform} from 'react-native';

export type CompletedFunction = (data: AuthWithSocialMutation) => void;
export type ErrorFunction = (error: ApolloError) => void;
export type SocialErrorFunction = (error: any) => void;

export interface useGoogleAuthProps {
  onCompleted: CompletedFunction;
  onError: ErrorFunction;
  onSocialError: SocialErrorFunction;
}

export const useGoogleAuth = ({
  onCompleted,
  onError,
  onSocialError,
}: useGoogleAuthProps) => {
  const [accessToken, setAccessToken] = useState<string>('');

  const updateAccessToken = (token: string) => {
    setAccessToken(token);
  };

  const [mutation] = useAuthWithSocialMutation({
    variables: {
      token: accessToken,
      socialType: Social_Type.Google,
    },
    onCompleted: data => onCompleted(data),
    onError: error => onError(error),
  });

  useEffect(() => {
    if (accessToken !== '') {
      mutation();
    }
  }, [accessToken, mutation]);

  GoogleSignin.configure({
    scopes: ['email', 'profile'],
    webClientId: Platform.select({
      ios:
        '944713835671-g37h5b6bktvn89qqnrva49e23in725ed.apps.googleusercontent.com',
      android:
        '1093765644772-7ien4addgn27l0qnquvshi7m6bl86h2t.apps.googleusercontent.com',
    }),
    offlineAccess: true,
    iosClientId: Platform.select({
      ios:
        '944713835671-g37h5b6bktvn89qqnrva49e23in725ed.apps.googleusercontent.com',
      android:
        '1093765644772-7ien4addgn27l0qnquvshi7m6bl86h2t.apps.googleusercontent.com',
    }),
  });

  const authWithGoogle = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      if (userInfo.idToken) {
        setAccessToken(userInfo.idToken);
      }
    } catch (error) {
      console.log(error);
      onSocialError(error);
    }
  };

  return {authWithGoogle};
};
