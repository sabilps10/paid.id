import {Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');

export const WindowDimensions = {
  width: width,
  height: height,
  width95: width * 0.95,
  width90: width * 0.9,
  width85: width * 0.85,
  width80: width * 0.8,
  width75: width * 0.75,
  width70: width * 0.7,
  width65: width * 0.65,
  width60: width * 0.6,
  width55: width * 0.55,
  width50: width * 0.5,
  width45: width * 0.45,
  width40: width * 0.4,
  width35: width * 0.35,
  width30: width * 0.3,
  width25: width * 0.25,
  width20: width * 0.2,
  width15: width * 0.15,
  width10: width * 0.1,
  width05: width * 0.05,
  width025: width * 0.025,
  width02: width * 0.02,
};
