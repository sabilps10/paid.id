import * as RNLocalize from 'react-native-localize';
import i18n from 'i18n-js';
import memoize from 'lodash.memoize';

const translationGetters = {
  en: () => require('../translations/en.json'),
  id: () => require('../translations/id.json'),
};

export const getI18nConfig = languageCode => {
  const fallback = {languageTag: 'id'};
  const {languageTag} = languageCode ? {languageTag: languageCode} : fallback;

  translateWithConfig.cache.clear();

  i18n.translations = {[languageTag]: translationGetters[languageTag]()};
  i18n.locale = languageTag;
};
export const translate = (key: string, languageCode: string): string => {
  const lan = getI18nConfig(languageCode);
  return translateWithConfig(key, lan);
};
export const translateWithConfig = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key),
);
