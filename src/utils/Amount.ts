export const calculateTotal = (item: any) => { 
  let total = item.products.reduce((prev: any, current: any) => { 
    return prev + current.price * current.quantity;
  }, 0);
  total -= total * item.discountPercent / 100;
  total += total * item.taxPercent / 100;
  total += item.shipping;
  return Math.round(total);
}
export const formatAmount = (amount: number) => { 
  //https://stackoverflow.com/questions/2901102/how-to-print-a-number-with-commas-as-thousands-separators-in-javascript
  return `Rp. ${amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
}