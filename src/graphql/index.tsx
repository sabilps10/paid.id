import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as React from 'react';
import * as ApolloReactComponents from '@apollo/react-components';
import * as ApolloReactHoc from '@apollo/react-hoc';
import * as ApolloReactHooks from '@apollo/react-hooks';
export type Maybe<T> = T | null;
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  DateTime: any;
  Upload: any;
};

export type AuthPayload = {
   __typename?: 'AuthPayload';
  user: User;
  token: Scalars['String'];
  tokenExpire: Scalars['DateTime'];
};


export type Invoice = {
   __typename?: 'Invoice';
  id: Scalars['ID'];
  clientName?: Maybe<Scalars['String']>;
  clinetAddress?: Maybe<Scalars['String']>;
  clientEmail?: Maybe<Scalars['String']>;
  invoiceNumber?: Maybe<Scalars['String']>;
  payTerm?: Maybe<Scalars['DateTime']>;
  products?: Maybe<Array<Maybe<Product>>>;
  taxPercent?: Maybe<Scalars['Float']>;
  discountPercent?: Maybe<Scalars['Float']>;
  comment?: Maybe<Scalars['String']>;
  pdfUrl?: Maybe<Scalars['String']>;
  subTotal?: Maybe<Scalars['Float']>;
  tax?: Maybe<Scalars['Float']>;
  total?: Maybe<Scalars['Float']>;
  shipping?: Maybe<Scalars['Float']>;
  status?: Maybe<Invoice_Status>;
};

export enum Invoice_Status {
  Created = 'CREATED',
  Deleted = 'DELETED'
}

export type InvoiceInput = {
  clientName?: Maybe<Scalars['String']>;
  clinetAddress?: Maybe<Scalars['String']>;
  clientEmail?: Maybe<Scalars['String']>;
  invoiceNumber?: Maybe<Scalars['String']>;
  payTerm?: Maybe<Scalars['DateTime']>;
  products?: Maybe<Array<Maybe<ProductInput>>>;
  taxPercent?: Maybe<Scalars['Float']>;
  discountPercent?: Maybe<Scalars['Float']>;
  shipping?: Maybe<Scalars['Float']>;
  comment?: Maybe<Scalars['String']>;
  localInvoiceId?: Maybe<Scalars['String']>;
  invoiceLocalPath?: Maybe<Scalars['String']>;
  pdfUrl?: Maybe<Scalars['String']>;
};

export type InvoiceNumberPayload = {
   __typename?: 'InvoiceNumberPayload';
  invoiceNumber?: Maybe<Scalars['String']>;
};

export type Issue = {
   __typename?: 'Issue';
  id: Scalars['ID'];
  user: User;
  title?: Maybe<Scalars['String']>;
  message?: Maybe<Scalars['String']>;
};

export type IssueInput = {
  title?: Maybe<Scalars['String']>;
  message?: Maybe<Scalars['String']>;
};

export type MePayload = {
   __typename?: 'MePayload';
  user?: Maybe<User>;
  invoices?: Maybe<Array<Maybe<Invoice>>>;
};

export type Mutation = {
   __typename?: 'Mutation';
  signUp: AuthPayload;
  logIn: AuthPayload;
  authWithSocial: AuthPayload;
  updateUserData: Result;
  createInvoice: Result;
  deleteInvoice: Result;
  createIssue: Result;
};


export type MutationSignUpArgs = {
  email: Scalars['String'];
  companyName: Scalars['String'];
  password: Scalars['String'];
};


export type MutationLogInArgs = {
  email: Scalars['String'];
  password: Scalars['String'];
};


export type MutationAuthWithSocialArgs = {
  token?: Maybe<Scalars['String']>;
  socialType?: Maybe<Social_Type>;
};


export type MutationUpdateUserDataArgs = {
  data?: Maybe<UpdateUserDataInput>;
};


export type MutationCreateInvoiceArgs = {
  data: InvoiceInput;
  pdf: Scalars['String'];
};


export type MutationDeleteInvoiceArgs = {
  invoiceNumber: Scalars['String'];
};


export type MutationCreateIssueArgs = {
  title: Scalars['String'];
  message: Scalars['String'];
};

export type Product = {
   __typename?: 'Product';
  id: Scalars['ID'];
  invoice: Invoice;
  name?: Maybe<Scalars['String']>;
  quantity?: Maybe<Scalars['Int']>;
  price?: Maybe<Scalars['Int']>;
};

export type ProductInput = {
  name?: Maybe<Scalars['String']>;
  quantity?: Maybe<Scalars['Int']>;
  price?: Maybe<Scalars['Int']>;
};

export type Query = {
   __typename?: 'Query';
  users?: Maybe<Array<Maybe<User>>>;
  me: MePayload;
  invoiceNumber: InvoiceNumberPayload;
};

export type Result = {
   __typename?: 'Result';
  isSuccess: Scalars['Boolean'];
  message?: Maybe<Scalars['String']>;
};

export enum Social_Type {
  Facebook = 'FACEBOOK',
  Google = 'GOOGLE'
}

export type Subscription = {
   __typename?: 'Subscription';
  newUser: User;
};

export type UpdateUserDataInput = {
  companyName?: Maybe<Scalars['String']>;
  companyAddress?: Maybe<Scalars['String']>;
  companyNumber?: Maybe<Scalars['String']>;
  companyWebsite?: Maybe<Scalars['String']>;
  companyEmailAdress?: Maybe<Scalars['String']>;
};


export type User = {
   __typename?: 'User';
  id: Scalars['ID'];
  email: Scalars['String'];
  firstName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  socialId?: Maybe<Scalars['String']>;
  companyName?: Maybe<Scalars['String']>;
  companyAddress?: Maybe<Scalars['String']>;
  companyNumber?: Maybe<Scalars['String']>;
  companyWebsite?: Maybe<Scalars['String']>;
  companyEmailAdress?: Maybe<Scalars['String']>;
};

export type UserFragmentFragment = (
  { __typename?: 'User' }
  & Pick<User, 'id' | 'firstName' | 'lastName' | 'companyName' | 'email' | 'socialId' | 'companyAddress' | 'companyNumber' | 'companyWebsite' | 'companyEmailAdress'>
);

export type InvoiceFragmentFragment = (
  { __typename?: 'Invoice' }
  & Pick<Invoice, 'id' | 'clientName' | 'clinetAddress' | 'clientEmail' | 'invoiceNumber' | 'payTerm' | 'taxPercent' | 'discountPercent' | 'shipping' | 'comment' | 'pdfUrl' | 'subTotal' | 'tax' | 'total'>
  & { products?: Maybe<Array<Maybe<(
    { __typename?: 'Product' }
    & ProductFragmentFragment
  )>>> }
);

export type ProductFragmentFragment = (
  { __typename?: 'Product' }
  & Pick<Product, 'id' | 'name' | 'quantity' | 'price'>
);

export type ResultFragmentFragment = (
  { __typename?: 'Result' }
  & Pick<Result, 'isSuccess' | 'message'>
);

export type SignUpMutationVariables = {
  companyName: Scalars['String'];
  email: Scalars['String'];
  password: Scalars['String'];
};


export type SignUpMutation = (
  { __typename?: 'Mutation' }
  & { signUp: (
    { __typename?: 'AuthPayload' }
    & Pick<AuthPayload, 'token' | 'tokenExpire'>
    & { user: (
      { __typename?: 'User' }
      & UserFragmentFragment
    ) }
  ) }
);

export type LogInMutationVariables = {
  email: Scalars['String'];
  password: Scalars['String'];
};


export type LogInMutation = (
  { __typename?: 'Mutation' }
  & { logIn: (
    { __typename?: 'AuthPayload' }
    & Pick<AuthPayload, 'token' | 'tokenExpire'>
    & { user: (
      { __typename?: 'User' }
      & UserFragmentFragment
    ) }
  ) }
);

export type AuthWithSocialMutationVariables = {
  token: Scalars['String'];
  socialType?: Maybe<Social_Type>;
};


export type AuthWithSocialMutation = (
  { __typename?: 'Mutation' }
  & { authWithSocial: (
    { __typename?: 'AuthPayload' }
    & Pick<AuthPayload, 'token' | 'tokenExpire'>
    & { user: (
      { __typename?: 'User' }
      & UserFragmentFragment
    ) }
  ) }
);

export type CreateInvoiceMutationVariables = {
  data: InvoiceInput;
  pdf: Scalars['String'];
};


export type CreateInvoiceMutation = (
  { __typename?: 'Mutation' }
  & { createInvoice: (
    { __typename?: 'Result' }
    & ResultFragmentFragment
  ) }
);

export type DeleteInvoiceMutationVariables = {
  invoiceNumber: Scalars['String'];
};


export type DeleteInvoiceMutation = (
  { __typename?: 'Mutation' }
  & { deleteInvoice: (
    { __typename?: 'Result' }
    & ResultFragmentFragment
  ) }
);

export type CreateIssueMutationVariables = {
  title: Scalars['String'];
  message: Scalars['String'];
};


export type CreateIssueMutation = (
  { __typename?: 'Mutation' }
  & { createIssue: (
    { __typename?: 'Result' }
    & ResultFragmentFragment
  ) }
);

export type UpdateUserDataMutationVariables = {
  data: UpdateUserDataInput;
};


export type UpdateUserDataMutation = (
  { __typename?: 'Mutation' }
  & { updateUserData: (
    { __typename?: 'Result' }
    & Pick<Result, 'isSuccess' | 'message'>
  ) }
);

export type InvoiceNumberQueryVariables = {};


export type InvoiceNumberQuery = (
  { __typename?: 'Query' }
  & { invoiceNumber: (
    { __typename?: 'InvoiceNumberPayload' }
    & Pick<InvoiceNumberPayload, 'invoiceNumber'>
  ) }
);

export type MeQueryVariables = {};


export type MeQuery = (
  { __typename?: 'Query' }
  & { me: (
    { __typename?: 'MePayload' }
    & { user?: Maybe<(
      { __typename?: 'User' }
      & UserFragmentFragment
    )>, invoices?: Maybe<Array<Maybe<(
      { __typename?: 'Invoice' }
      & InvoiceFragmentFragment
    )>>> }
  ) }
);

export const UserFragmentFragmentDoc = gql`
    fragment UserFragment on User {
  id
  firstName
  lastName
  companyName
  email
  socialId
  companyAddress
  companyNumber
  companyWebsite
  companyEmailAdress
}
    `;
export const ProductFragmentFragmentDoc = gql`
    fragment ProductFragment on Product {
  id
  name
  quantity
  price
}
    `;
export const InvoiceFragmentFragmentDoc = gql`
    fragment InvoiceFragment on Invoice {
  id
  clientName
  clinetAddress
  clientEmail
  invoiceNumber
  payTerm
  products {
    ...ProductFragment
  }
  taxPercent
  discountPercent
  shipping
  comment
  pdfUrl
  subTotal
  tax
  total
}
    ${ProductFragmentFragmentDoc}`;
export const ResultFragmentFragmentDoc = gql`
    fragment ResultFragment on Result {
  isSuccess
  message
}
    `;
export const SignUpDocument = gql`
    mutation signUp($companyName: String!, $email: String!, $password: String!) {
  signUp(companyName: $companyName, email: $email, password: $password) {
    user {
      ...UserFragment
    }
    token
    tokenExpire
  }
}
    ${UserFragmentFragmentDoc}`;
export type SignUpMutationFn = ApolloReactCommon.MutationFunction<SignUpMutation, SignUpMutationVariables>;
export type SignUpComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<SignUpMutation, SignUpMutationVariables>, 'mutation'>;

    export const SignUpComponent = (props: SignUpComponentProps) => (
      <ApolloReactComponents.Mutation<SignUpMutation, SignUpMutationVariables> mutation={SignUpDocument} {...props} />
    );
    
export type SignUpProps<TChildProps = {}, TDataName extends string = 'mutate'> = {
      [key in TDataName]: ApolloReactCommon.MutationFunction<SignUpMutation, SignUpMutationVariables>
    } & TChildProps;
export function withSignUp<TProps, TChildProps = {}, TDataName extends string = 'mutate'>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  SignUpMutation,
  SignUpMutationVariables,
  SignUpProps<TChildProps, TDataName>>) {
    return ApolloReactHoc.withMutation<TProps, SignUpMutation, SignUpMutationVariables, SignUpProps<TChildProps, TDataName>>(SignUpDocument, {
      alias: 'signUp',
      ...operationOptions
    });
};

/**
 * __useSignUpMutation__
 *
 * To run a mutation, you first call `useSignUpMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSignUpMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [signUpMutation, { data, loading, error }] = useSignUpMutation({
 *   variables: {
 *      companyName: // value for 'companyName'
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useSignUpMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<SignUpMutation, SignUpMutationVariables>) {
        return ApolloReactHooks.useMutation<SignUpMutation, SignUpMutationVariables>(SignUpDocument, baseOptions);
      }
export type SignUpMutationHookResult = ReturnType<typeof useSignUpMutation>;
export type SignUpMutationResult = ApolloReactCommon.MutationResult<SignUpMutation>;
export type SignUpMutationOptions = ApolloReactCommon.BaseMutationOptions<SignUpMutation, SignUpMutationVariables>;
export const LogInDocument = gql`
    mutation logIn($email: String!, $password: String!) {
  logIn(email: $email, password: $password) {
    user {
      ...UserFragment
    }
    token
    tokenExpire
  }
}
    ${UserFragmentFragmentDoc}`;
export type LogInMutationFn = ApolloReactCommon.MutationFunction<LogInMutation, LogInMutationVariables>;
export type LogInComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<LogInMutation, LogInMutationVariables>, 'mutation'>;

    export const LogInComponent = (props: LogInComponentProps) => (
      <ApolloReactComponents.Mutation<LogInMutation, LogInMutationVariables> mutation={LogInDocument} {...props} />
    );
    
export type LogInProps<TChildProps = {}, TDataName extends string = 'mutate'> = {
      [key in TDataName]: ApolloReactCommon.MutationFunction<LogInMutation, LogInMutationVariables>
    } & TChildProps;
export function withLogIn<TProps, TChildProps = {}, TDataName extends string = 'mutate'>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  LogInMutation,
  LogInMutationVariables,
  LogInProps<TChildProps, TDataName>>) {
    return ApolloReactHoc.withMutation<TProps, LogInMutation, LogInMutationVariables, LogInProps<TChildProps, TDataName>>(LogInDocument, {
      alias: 'logIn',
      ...operationOptions
    });
};

/**
 * __useLogInMutation__
 *
 * To run a mutation, you first call `useLogInMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLogInMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [logInMutation, { data, loading, error }] = useLogInMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLogInMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<LogInMutation, LogInMutationVariables>) {
        return ApolloReactHooks.useMutation<LogInMutation, LogInMutationVariables>(LogInDocument, baseOptions);
      }
export type LogInMutationHookResult = ReturnType<typeof useLogInMutation>;
export type LogInMutationResult = ApolloReactCommon.MutationResult<LogInMutation>;
export type LogInMutationOptions = ApolloReactCommon.BaseMutationOptions<LogInMutation, LogInMutationVariables>;
export const AuthWithSocialDocument = gql`
    mutation authWithSocial($token: String!, $socialType: SOCIAL_TYPE) {
  authWithSocial(token: $token, socialType: $socialType) {
    user {
      ...UserFragment
    }
    token
    tokenExpire
  }
}
    ${UserFragmentFragmentDoc}`;
export type AuthWithSocialMutationFn = ApolloReactCommon.MutationFunction<AuthWithSocialMutation, AuthWithSocialMutationVariables>;
export type AuthWithSocialComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<AuthWithSocialMutation, AuthWithSocialMutationVariables>, 'mutation'>;

    export const AuthWithSocialComponent = (props: AuthWithSocialComponentProps) => (
      <ApolloReactComponents.Mutation<AuthWithSocialMutation, AuthWithSocialMutationVariables> mutation={AuthWithSocialDocument} {...props} />
    );
    
export type AuthWithSocialProps<TChildProps = {}, TDataName extends string = 'mutate'> = {
      [key in TDataName]: ApolloReactCommon.MutationFunction<AuthWithSocialMutation, AuthWithSocialMutationVariables>
    } & TChildProps;
export function withAuthWithSocial<TProps, TChildProps = {}, TDataName extends string = 'mutate'>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  AuthWithSocialMutation,
  AuthWithSocialMutationVariables,
  AuthWithSocialProps<TChildProps, TDataName>>) {
    return ApolloReactHoc.withMutation<TProps, AuthWithSocialMutation, AuthWithSocialMutationVariables, AuthWithSocialProps<TChildProps, TDataName>>(AuthWithSocialDocument, {
      alias: 'authWithSocial',
      ...operationOptions
    });
};

/**
 * __useAuthWithSocialMutation__
 *
 * To run a mutation, you first call `useAuthWithSocialMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAuthWithSocialMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [authWithSocialMutation, { data, loading, error }] = useAuthWithSocialMutation({
 *   variables: {
 *      token: // value for 'token'
 *      socialType: // value for 'socialType'
 *   },
 * });
 */
export function useAuthWithSocialMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<AuthWithSocialMutation, AuthWithSocialMutationVariables>) {
        return ApolloReactHooks.useMutation<AuthWithSocialMutation, AuthWithSocialMutationVariables>(AuthWithSocialDocument, baseOptions);
      }
export type AuthWithSocialMutationHookResult = ReturnType<typeof useAuthWithSocialMutation>;
export type AuthWithSocialMutationResult = ApolloReactCommon.MutationResult<AuthWithSocialMutation>;
export type AuthWithSocialMutationOptions = ApolloReactCommon.BaseMutationOptions<AuthWithSocialMutation, AuthWithSocialMutationVariables>;
export const CreateInvoiceDocument = gql`
    mutation createInvoice($data: InvoiceInput!, $pdf: String!) {
  createInvoice(data: $data, pdf: $pdf) {
    ...ResultFragment
  }
}
    ${ResultFragmentFragmentDoc}`;
export type CreateInvoiceMutationFn = ApolloReactCommon.MutationFunction<CreateInvoiceMutation, CreateInvoiceMutationVariables>;
export type CreateInvoiceComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<CreateInvoiceMutation, CreateInvoiceMutationVariables>, 'mutation'>;

    export const CreateInvoiceComponent = (props: CreateInvoiceComponentProps) => (
      <ApolloReactComponents.Mutation<CreateInvoiceMutation, CreateInvoiceMutationVariables> mutation={CreateInvoiceDocument} {...props} />
    );
    
export type CreateInvoiceProps<TChildProps = {}, TDataName extends string = 'mutate'> = {
      [key in TDataName]: ApolloReactCommon.MutationFunction<CreateInvoiceMutation, CreateInvoiceMutationVariables>
    } & TChildProps;
export function withCreateInvoice<TProps, TChildProps = {}, TDataName extends string = 'mutate'>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  CreateInvoiceMutation,
  CreateInvoiceMutationVariables,
  CreateInvoiceProps<TChildProps, TDataName>>) {
    return ApolloReactHoc.withMutation<TProps, CreateInvoiceMutation, CreateInvoiceMutationVariables, CreateInvoiceProps<TChildProps, TDataName>>(CreateInvoiceDocument, {
      alias: 'createInvoice',
      ...operationOptions
    });
};

/**
 * __useCreateInvoiceMutation__
 *
 * To run a mutation, you first call `useCreateInvoiceMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateInvoiceMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createInvoiceMutation, { data, loading, error }] = useCreateInvoiceMutation({
 *   variables: {
 *      data: // value for 'data'
 *      pdf: // value for 'pdf'
 *   },
 * });
 */
export function useCreateInvoiceMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CreateInvoiceMutation, CreateInvoiceMutationVariables>) {
        return ApolloReactHooks.useMutation<CreateInvoiceMutation, CreateInvoiceMutationVariables>(CreateInvoiceDocument, baseOptions);
      }
export type CreateInvoiceMutationHookResult = ReturnType<typeof useCreateInvoiceMutation>;
export type CreateInvoiceMutationResult = ApolloReactCommon.MutationResult<CreateInvoiceMutation>;
export type CreateInvoiceMutationOptions = ApolloReactCommon.BaseMutationOptions<CreateInvoiceMutation, CreateInvoiceMutationVariables>;
export const DeleteInvoiceDocument = gql`
    mutation deleteInvoice($invoiceNumber: String!) {
  deleteInvoice(invoiceNumber: $invoiceNumber) {
    ...ResultFragment
  }
}
    ${ResultFragmentFragmentDoc}`;
export type DeleteInvoiceMutationFn = ApolloReactCommon.MutationFunction<DeleteInvoiceMutation, DeleteInvoiceMutationVariables>;
export type DeleteInvoiceComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<DeleteInvoiceMutation, DeleteInvoiceMutationVariables>, 'mutation'>;

    export const DeleteInvoiceComponent = (props: DeleteInvoiceComponentProps) => (
      <ApolloReactComponents.Mutation<DeleteInvoiceMutation, DeleteInvoiceMutationVariables> mutation={DeleteInvoiceDocument} {...props} />
    );
    
export type DeleteInvoiceProps<TChildProps = {}, TDataName extends string = 'mutate'> = {
      [key in TDataName]: ApolloReactCommon.MutationFunction<DeleteInvoiceMutation, DeleteInvoiceMutationVariables>
    } & TChildProps;
export function withDeleteInvoice<TProps, TChildProps = {}, TDataName extends string = 'mutate'>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  DeleteInvoiceMutation,
  DeleteInvoiceMutationVariables,
  DeleteInvoiceProps<TChildProps, TDataName>>) {
    return ApolloReactHoc.withMutation<TProps, DeleteInvoiceMutation, DeleteInvoiceMutationVariables, DeleteInvoiceProps<TChildProps, TDataName>>(DeleteInvoiceDocument, {
      alias: 'deleteInvoice',
      ...operationOptions
    });
};

/**
 * __useDeleteInvoiceMutation__
 *
 * To run a mutation, you first call `useDeleteInvoiceMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteInvoiceMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteInvoiceMutation, { data, loading, error }] = useDeleteInvoiceMutation({
 *   variables: {
 *      invoiceNumber: // value for 'invoiceNumber'
 *   },
 * });
 */
export function useDeleteInvoiceMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<DeleteInvoiceMutation, DeleteInvoiceMutationVariables>) {
        return ApolloReactHooks.useMutation<DeleteInvoiceMutation, DeleteInvoiceMutationVariables>(DeleteInvoiceDocument, baseOptions);
      }
export type DeleteInvoiceMutationHookResult = ReturnType<typeof useDeleteInvoiceMutation>;
export type DeleteInvoiceMutationResult = ApolloReactCommon.MutationResult<DeleteInvoiceMutation>;
export type DeleteInvoiceMutationOptions = ApolloReactCommon.BaseMutationOptions<DeleteInvoiceMutation, DeleteInvoiceMutationVariables>;
export const CreateIssueDocument = gql`
    mutation createIssue($title: String!, $message: String!) {
  createIssue(title: $title, message: $message) {
    ...ResultFragment
  }
}
    ${ResultFragmentFragmentDoc}`;
export type CreateIssueMutationFn = ApolloReactCommon.MutationFunction<CreateIssueMutation, CreateIssueMutationVariables>;
export type CreateIssueComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<CreateIssueMutation, CreateIssueMutationVariables>, 'mutation'>;

    export const CreateIssueComponent = (props: CreateIssueComponentProps) => (
      <ApolloReactComponents.Mutation<CreateIssueMutation, CreateIssueMutationVariables> mutation={CreateIssueDocument} {...props} />
    );
    
export type CreateIssueProps<TChildProps = {}, TDataName extends string = 'mutate'> = {
      [key in TDataName]: ApolloReactCommon.MutationFunction<CreateIssueMutation, CreateIssueMutationVariables>
    } & TChildProps;
export function withCreateIssue<TProps, TChildProps = {}, TDataName extends string = 'mutate'>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  CreateIssueMutation,
  CreateIssueMutationVariables,
  CreateIssueProps<TChildProps, TDataName>>) {
    return ApolloReactHoc.withMutation<TProps, CreateIssueMutation, CreateIssueMutationVariables, CreateIssueProps<TChildProps, TDataName>>(CreateIssueDocument, {
      alias: 'createIssue',
      ...operationOptions
    });
};

/**
 * __useCreateIssueMutation__
 *
 * To run a mutation, you first call `useCreateIssueMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateIssueMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createIssueMutation, { data, loading, error }] = useCreateIssueMutation({
 *   variables: {
 *      title: // value for 'title'
 *      message: // value for 'message'
 *   },
 * });
 */
export function useCreateIssueMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CreateIssueMutation, CreateIssueMutationVariables>) {
        return ApolloReactHooks.useMutation<CreateIssueMutation, CreateIssueMutationVariables>(CreateIssueDocument, baseOptions);
      }
export type CreateIssueMutationHookResult = ReturnType<typeof useCreateIssueMutation>;
export type CreateIssueMutationResult = ApolloReactCommon.MutationResult<CreateIssueMutation>;
export type CreateIssueMutationOptions = ApolloReactCommon.BaseMutationOptions<CreateIssueMutation, CreateIssueMutationVariables>;
export const UpdateUserDataDocument = gql`
    mutation updateUserData($data: UpdateUserDataInput!) {
  updateUserData(data: $data) {
    isSuccess
    message
  }
}
    `;
export type UpdateUserDataMutationFn = ApolloReactCommon.MutationFunction<UpdateUserDataMutation, UpdateUserDataMutationVariables>;
export type UpdateUserDataComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<UpdateUserDataMutation, UpdateUserDataMutationVariables>, 'mutation'>;

    export const UpdateUserDataComponent = (props: UpdateUserDataComponentProps) => (
      <ApolloReactComponents.Mutation<UpdateUserDataMutation, UpdateUserDataMutationVariables> mutation={UpdateUserDataDocument} {...props} />
    );
    
export type UpdateUserDataProps<TChildProps = {}, TDataName extends string = 'mutate'> = {
      [key in TDataName]: ApolloReactCommon.MutationFunction<UpdateUserDataMutation, UpdateUserDataMutationVariables>
    } & TChildProps;
export function withUpdateUserData<TProps, TChildProps = {}, TDataName extends string = 'mutate'>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  UpdateUserDataMutation,
  UpdateUserDataMutationVariables,
  UpdateUserDataProps<TChildProps, TDataName>>) {
    return ApolloReactHoc.withMutation<TProps, UpdateUserDataMutation, UpdateUserDataMutationVariables, UpdateUserDataProps<TChildProps, TDataName>>(UpdateUserDataDocument, {
      alias: 'updateUserData',
      ...operationOptions
    });
};

/**
 * __useUpdateUserDataMutation__
 *
 * To run a mutation, you first call `useUpdateUserDataMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUserDataMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUserDataMutation, { data, loading, error }] = useUpdateUserDataMutation({
 *   variables: {
 *      data: // value for 'data'
 *   },
 * });
 */
export function useUpdateUserDataMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateUserDataMutation, UpdateUserDataMutationVariables>) {
        return ApolloReactHooks.useMutation<UpdateUserDataMutation, UpdateUserDataMutationVariables>(UpdateUserDataDocument, baseOptions);
      }
export type UpdateUserDataMutationHookResult = ReturnType<typeof useUpdateUserDataMutation>;
export type UpdateUserDataMutationResult = ApolloReactCommon.MutationResult<UpdateUserDataMutation>;
export type UpdateUserDataMutationOptions = ApolloReactCommon.BaseMutationOptions<UpdateUserDataMutation, UpdateUserDataMutationVariables>;
export const InvoiceNumberDocument = gql`
    query InvoiceNumber {
  invoiceNumber {
    invoiceNumber
  }
}
    `;
export type InvoiceNumberComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<InvoiceNumberQuery, InvoiceNumberQueryVariables>, 'query'>;

    export const InvoiceNumberComponent = (props: InvoiceNumberComponentProps) => (
      <ApolloReactComponents.Query<InvoiceNumberQuery, InvoiceNumberQueryVariables> query={InvoiceNumberDocument} {...props} />
    );
    
export type InvoiceNumberProps<TChildProps = {}, TDataName extends string = 'data'> = {
      [key in TDataName]: ApolloReactHoc.DataValue<InvoiceNumberQuery, InvoiceNumberQueryVariables>
    } & TChildProps;
export function withInvoiceNumber<TProps, TChildProps = {}, TDataName extends string = 'data'>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  InvoiceNumberQuery,
  InvoiceNumberQueryVariables,
  InvoiceNumberProps<TChildProps, TDataName>>) {
    return ApolloReactHoc.withQuery<TProps, InvoiceNumberQuery, InvoiceNumberQueryVariables, InvoiceNumberProps<TChildProps, TDataName>>(InvoiceNumberDocument, {
      alias: 'invoiceNumber',
      ...operationOptions
    });
};

/**
 * __useInvoiceNumberQuery__
 *
 * To run a query within a React component, call `useInvoiceNumberQuery` and pass it any options that fit your needs.
 * When your component renders, `useInvoiceNumberQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useInvoiceNumberQuery({
 *   variables: {
 *   },
 * });
 */
export function useInvoiceNumberQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<InvoiceNumberQuery, InvoiceNumberQueryVariables>) {
        return ApolloReactHooks.useQuery<InvoiceNumberQuery, InvoiceNumberQueryVariables>(InvoiceNumberDocument, baseOptions);
      }
export function useInvoiceNumberLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<InvoiceNumberQuery, InvoiceNumberQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<InvoiceNumberQuery, InvoiceNumberQueryVariables>(InvoiceNumberDocument, baseOptions);
        }
export type InvoiceNumberQueryHookResult = ReturnType<typeof useInvoiceNumberQuery>;
export type InvoiceNumberLazyQueryHookResult = ReturnType<typeof useInvoiceNumberLazyQuery>;
export type InvoiceNumberQueryResult = ApolloReactCommon.QueryResult<InvoiceNumberQuery, InvoiceNumberQueryVariables>;
export const MeDocument = gql`
    query Me {
  me {
    user {
      ...UserFragment
    }
    invoices {
      ...InvoiceFragment
    }
  }
}
    ${UserFragmentFragmentDoc}
${InvoiceFragmentFragmentDoc}`;
export type MeComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<MeQuery, MeQueryVariables>, 'query'>;

    export const MeComponent = (props: MeComponentProps) => (
      <ApolloReactComponents.Query<MeQuery, MeQueryVariables> query={MeDocument} {...props} />
    );
    
export type MeProps<TChildProps = {}, TDataName extends string = 'data'> = {
      [key in TDataName]: ApolloReactHoc.DataValue<MeQuery, MeQueryVariables>
    } & TChildProps;
export function withMe<TProps, TChildProps = {}, TDataName extends string = 'data'>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  MeQuery,
  MeQueryVariables,
  MeProps<TChildProps, TDataName>>) {
    return ApolloReactHoc.withQuery<TProps, MeQuery, MeQueryVariables, MeProps<TChildProps, TDataName>>(MeDocument, {
      alias: 'me',
      ...operationOptions
    });
};

/**
 * __useMeQuery__
 *
 * To run a query within a React component, call `useMeQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMeQuery({
 *   variables: {
 *   },
 * });
 */
export function useMeQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<MeQuery, MeQueryVariables>) {
        return ApolloReactHooks.useQuery<MeQuery, MeQueryVariables>(MeDocument, baseOptions);
      }
export function useMeLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<MeQuery, MeQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<MeQuery, MeQueryVariables>(MeDocument, baseOptions);
        }
export type MeQueryHookResult = ReturnType<typeof useMeQuery>;
export type MeLazyQueryHookResult = ReturnType<typeof useMeLazyQuery>;
export type MeQueryResult = ApolloReactCommon.QueryResult<MeQuery, MeQueryVariables>;