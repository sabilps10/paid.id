import React, {useState} from 'react';
import {BottomNavigation} from 'react-native-material-ui';
import {useNavigation} from '@react-navigation/native';

export const PaidBottomNavigation: React.FC = () => {
  const [activeNavigation, setActiveNavigation] = useState<string>('home');
  const navigation = useNavigation();

  return (
    <BottomNavigation
      style={{
        container: {
          position: 'absolute',
          bottom: 0,
          left: 0,
          right: 0,
          shadowOpacity: 0,
        },
      }}
      active={activeNavigation}
      hidden={false}>
      <BottomNavigation.Action
        key="home"
        icon="today"
        label="Dashboard"
        onPress={() => {
          setActiveNavigation('home');
          navigation.navigate('Home');
        }}
      />
      <BottomNavigation.Action
        key="invoice"
        icon="flip-to-front"
        label="Invoices"
        onPress={() => {
          setActiveNavigation('invoice');
          navigation.navigate('Invoice');
        }}
      />
      <BottomNavigation.Action
        key="settings"
        icon="settings"
        label="Settings"
        onPress={() => {
          setActiveNavigation('settings');
          navigation.navigate('Settings');
        }}
      />
    </BottomNavigation>
  );
};
