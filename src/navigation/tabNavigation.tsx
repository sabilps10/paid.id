import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {HomeNavigation} from '../screens/Home/navigation';
import {SettingsNavigation} from '../screens/Settings/navigation';
import {InvoiceNavigation} from '../screens/Invoice/navigation';
import {Icon} from 'react-native-material-ui';

const Tab = createBottomTabNavigator();

export const TabNavigation = () => {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={{tabBarVisible: true}}
        initialRouteName="Home">
        <Tab.Screen
          options={{
            tabBarIcon: () => <Icon name="today" />,
          }}
          name="Home"
          component={HomeNavigation}
        />
        <Tab.Screen
          options={{
            tabBarIcon: () => <Icon name="flip-to-front" />,
          }}
          name="Invoices"
          component={InvoiceNavigation}
        />
        <Tab.Screen
          options={{
            tabBarIcon: () => <Icon name="settings" />,
          }}
          name="Settings"
          component={SettingsNavigation}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
};
